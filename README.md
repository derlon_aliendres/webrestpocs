# WebRestPOCs

Os projetos deste repositório visam disseminar conhecimento e **melhores práticas/padrões** o que for possível para a sua assimilação.

No diretório 'Web' no projeto 'vraptor3-blank-MeuProject-Strategy', basicamente tem uma API **RESTFul** (ou WebService REST), implementada em **vRaptor** (que inclusive atende pág.s JSP **simultaneamente**), e chamadas  (implementadas em jQuery) que consomem estes endPoint's.

Em suma, no Server-side são implementado os Serviços Rest em Controller vRaptor3 que, ao mesmo tempo, também atendem páginas HTML submetendo dados em um **<form** (de forma similar a JSR**371**-MVC com a **JAX-RS2**).

Essa página HTML também usa a técnica de JSONificar o formulário HTML, só que neste caso, com o '`name`' do input seguindo a **convensão** vRaptor. Caso o JavaScript esteja habilitado, estão é dispara um chamada AJAX (consumido o endPoint's Rest).

___  
--- 
Instruções básicas:

click no link `'cliente/manter'` -> link '`manter`' -> cadastre clientes a vontade (dica: adicione tambem nomes 'Test', 'Test2', etc. para testar o filtro '`Pesquisar`'; p/ex.: com 'Test')
Experimente também o UC '`Produto`'.


