/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.schlimmspringcdi.interceptscope;

import br.com.vitoria.schlimmspringcdi.interceptScope.Simple_MyServiceInterface;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author derlon.aliendres
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
@DirtiesContext
public class SimpleInterceptorTestCase {

    @Inject //@Autowired will be intercepted
    private Simple_MyServiceInterface someInterceptedBean;

    @Test
    public void testReturnValueInterceptarSayHelloExtension() {
        Assert.assertTrue(someInterceptedBean.sayHello().equals("Hello_hello_world"));
        Logger.getLogger(this.getClass().getName() ).log(Level.INFO, "Test do Interceptor passou!");
    }
	
}

