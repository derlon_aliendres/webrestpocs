/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.schlimmspringcdi.interceptScope;

/**
 *
 * @author derlon.aliendres
 */
public interface Simple_MyServiceInterface {

	String sayHello();
	String sayHello(String what);
	String sayGoodBye();
	
}
