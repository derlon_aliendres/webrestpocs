/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.schlimmspringcdi.interceptScope;

import br.com.vitoria.schlimmspringcdi.interceptScope.dip.MyService;
import org.springframework.stereotype.Component;

/**
 *
 * @author derlon.aliendres
 */
@Component 
public class MyServiceImpl implements MyService { 
  
 @Override
 public String sayHello() { 
    return "Hello"; 
 } 
  
} 
