package br.com.vitoria.schlimmspringcdi.interceptScope.dip;

/**
 *
 * @author derlon.aliendres
 */
public interface MyService { 
  
 String sayHello(); 
  
 } 
