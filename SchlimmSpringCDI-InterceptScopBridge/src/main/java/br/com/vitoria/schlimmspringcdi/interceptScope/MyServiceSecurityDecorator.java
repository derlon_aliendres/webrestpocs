/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.schlimmspringcdi.interceptScope;

import br.com.vitoria.schlimmspringcdi.interceptScope.dip.MyService;
import javax.decorator.Decorator;
import javax.decorator.Delegate;

/**
 *
 * @author derlon.aliendres
 */
@Decorator 
public class MyServiceSecurityDecorator implements MyService { 
  
 @Delegate 
 private MyService delegate; 
  
 public String sayHello() { 
 // do some security stuff 
 return delegate.sayHello(); 
 } 
  
} 
