/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250;

//import br.com.vitoria.springSecJSR250.config.ConfigTestUtilsEnum;
//import br.com.vitoria.springSecJSR250.config.InMemoryXmlApplicationContext;
import br.com.vitoria.springSecJSR250.config.UsernamePasswordWithTimeoutAuthenticationToken;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.authentication.event.LoggerListener;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author derlon.aliendres
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
@org.junit.FixMethodOrder(MethodSorters.NAME_ASCENDING)//->>Ordem: a1, a2, a3, ... 
public class Jsr250AnnotationDomainBizzServiceTest {

//    private static InMemoryXmlApplicationContext appContext; 
    @Inject // @Resource // @Autowired
    private /*static*/ BusinessService target; 
    @Inject // 
    private ApplicationEventPublisher  _eventPublisher; 
    @Inject // 
    private DaoAuthenticationProvider  daoAuthenticationProvider; // AuthenticationProvider _authProvider

    @BeforeClass
    public static void setUpClass() {
//        appContext = new InMemoryXmlApplicationContext( 
//            //    "<b:bean id='target' class='org.springframework.security.access.annotation.Jsr250BusinessServiceImpl'/>" + 
//               " <context:component-scan base-package=\"br.com.vitoria.springSecJSR250\" />" + 
//                "<global-method-security jsr250-annotations='enabled'/>" + ConfigTestUtilsEnum.AUTH_PROV_TEST_TOKEN 
//                ); 
//        target = (BusinessService)appContext.getBean(BusinessService.class/*"target"*/); 
    }
    
    @Before 
    public void loadAppContext() { 
    } 

    @After 
    public void clearSecurityContext() { 
        SecurityContextHolder.clearContext(); 
    } 
    
    @AfterClass
    public static void tearDownClass() {
//        if (appContext != null) { 
//            appContext.close();//<<-Como o contexto é carregado só 1x(no setUpClass) só pode ser encerrado aki! 
//        } 
    }


    @Test(expected=AuthenticationCredentialsNotFoundException.class) 
    public void a1targetShouldPreventProtectedMethodInvocationWithNoContext() { 
        target.someUserMethod1(); //<<<--incide aqui o teste!!!
        System.out.println("Falha na Autenticação: fluxo exec BLOQUEADO!!!)");
    } 

    @Test 
    public void a2permitAllShouldBeDefaultAttribute() { 
        UsernamePasswordAuthenticationToken token
            = new UsernamePasswordAuthenticationToken("Test", "Password", AuthorityUtils.createAuthorityList("ROLE_USER") ); 
        SecurityContextHolder.getContext().setAuthentication(token);// SecurityContextHolder.getContextHolderStrategy().getContext().getAuthentication().
//        LoggerListener listener = new LoggerListener();.onApplicationEvent(event);
        _eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(token, this.getClass() ) );

        target.someOther(0); //<<<--incide aqui o teste!!!
    } 

    @Test 
    public void a3targetShouldAllowProtectedMethodInvocationWithCorrectRole() { 
        UsernamePasswordAuthenticationToken token
            = new UsernamePasswordAuthenticationToken("Test", "Password", AuthorityUtils.createAuthorityList("ROLE_USER")); 
        SecurityContextHolder.getContext().setAuthentication(token); 
        _eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(token, this.getClass() ) );

        target.someUserMethod1(); //<<<--incide aqui o teste!!!
    } 

    @Inject
    private SessionRegistry sessionRegistry;
    @Test(expected=AccountExpiredException/*AccessDeniedException*/.class) // ProviderNotFoundException
    public void a3targetShouldPreventInvocationWithCorrectRoleButNoLongerAuthenticated() { 
        final String timeOut = "0010";
        final Date datimeLastRequest = new Date()/*, AuthorityUtils.stringArrayToAuthorityArray(new String[]{"ROLE_USER"}) */;
        UsernamePasswordWithTimeoutAuthenticationToken token
                = new UsernamePasswordWithTimeoutAuthenticationToken("Test", "Password", timeOut, "001", datimeLastRequest); 
//        UsernamePasswordAuthenticationToken token
//                = new UsernamePasswordAuthenticationToken("Test", "Password"/*, AuthorityUtils.stringArrayToAuthorityArray(new String[]{"ROLE_USER"})*/); 
        Authentication auth = /*(StandardSessionAuthenticationToken)*/daoAuthenticationProvider.authenticate(token); // ;
        token.setAuthorities(new ArrayList<GrantedAuthority>(auth.getAuthorities() ) );
        SecurityContextHolder.getContext().setAuthentication( /*(UsernamePasswordWithTimeoutAuthenticationToken*/token); //auth.getDetails() 
        _eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(token, this.getClass() ) ); // token
    /*    SecurityContextHolder.getContext().getAuthentication()token.setAuthenticated(false)*//*.eraseCredentials()*/;
        try {
            /*new */Thread.sleep(30/*00*/);//<<<--Idle time of user Session Simulation
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        SecurityContext sc = SecurityContextHolder.getContext();
//
//        String idSessao = SessionRegistryUtils.obtainSessionIdFromAuthentication(sc.getAuthentication());
//        sessionRegistry.registerNewSession(idSessao, auth.getPrincipal())/*getName()*//*.get(0).expireNow()*/;
//    //    System.out.println("SessionInformation na Autenticação: " + idSessao);
//        SessionInformation[] sessoes = sessionRegistry.getAllSessions(auth.getPrincipal(), true); // sc.getAuthentication()
//        for (SessionInformation session: sessoes) {
//            int i = 1;
//            System.out.println("SessionInformation na Autenticação: " + i++ + "º: " + session.getSessionId() );
//        }
//        auth = null;
        try {
            target.someUserMethod1(); //<<<--incide aqui o teste!!!
        } catch (Throwable e) {
            if (e.getClass().isAssignableFrom(AccountExpiredException.class) ) {
                throw e;
            }
            e.printStackTrace();
        }
//        token.setAuthenticated(false);
    //    SecurityContextHolder.getContext().setAuthentication(token); //auth.getDetails() 
        target.someUserMethod1(); //<<<--incide aqui o teste!!!
    } 

    @Test(expected=AccessDeniedException.class)
    public void a4targetShouldPreventProtectedMethodInvocationWithIncorrectRole() { 
        TestingAuthenticationToken token
                = new TestingAuthenticationToken("Test", "Password", /*AuthorityUtils.createAuthorityList(*/"ROLE_SOMEINVALIDROLE"/*)*/); 
        SecurityContextHolder.getContext().setAuthentication(token); 
        _eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(token, this.getClass() ) );

        target.someAdminMethod(); //<<<--incide aqui o teste!!!
        System.out.println("Falha na Autenticação: fluxo exec BLOQUEADO!!!)");
    } 
} 
