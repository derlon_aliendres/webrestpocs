/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250.config;

import java.util.Collection;
import java.util.Date;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Derlon.Aliendres
 */
public class UsernamePasswordWithTimeoutAuthenticationToken extends StandardSessionAuthenticationToken
                                                                   /* UsernamePasswordAuthenticationToken */{
	private String timeout = null;

	public UsernamePasswordWithTimeoutAuthenticationToken(Object principal, Object credentials, String timeOut
                                                              , String sessionId, Date lastRequest) {
            this(principal, credentials, sessionId, lastRequest, timeOut, null);
        //    this.timeout = null;
	}

    /**
     *
     * @param principal the value of principal
     * @param credentials the value of credentials
     * @param sessionId the value of sessionId
     * @param lastRequest the value of lastRequest
     * @param timeOut the value of timeOut
     * @param authorities the value of authorities
     */
    public UsernamePasswordWithTimeoutAuthenticationToken(Object principal, Object credentials, String sessionId, Date lastRequest
                                                          , String timeOut, Collection</*? extends */GrantedAuthority> authorities) {
            super(principal, credentials, sessionId, lastRequest, authorities);
            this.timeout = timeOut;
	}
	
	public String getTimeout() {
		return timeout;
	}	

}