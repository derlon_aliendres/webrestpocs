/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250.config;

/**
 *
 * @author derlon.aliendres
 */
public enum ConfigTestUtilsEnum { 
    AUTH_PROVIDER_USERPSWD("<authentication-manager alias='authManager'>" + 
       "    <authentication-provider \">\n" + 
       "        <user-service id='us'>" + 
       "            <user name='bob' password='bobspassword' authorities='ROLE_A,ROLE_B' />" + 
       "            <user name='bill' password='billspassword' authorities='ROLE_A,ROLE_B,AUTH_OTHER' />" + 
        "            <user name='admin' password='password' authorities='ROLE_ADMIN,ROLE_USER' />" + 
        "            <user name='user' password='password' authorities='ROLE_USER' />" + 
        "        </user-service>" + 
        "    </authentication-provider>" + 
        "</authentication-manager>")
    , AUTH_PROV_TEST_TOKEN("<authentication-manager alias='authManager'>\n"
                           + "    <authentication-provider  ref=\"testingAuthenticationProvider\">\n"
                           + "    </authentication-provider>\n"
                           + "    </authentication-manager>\n"
                           + "<b:bean id=\"testingAuthenticationProvider\" class=\"org.springframework.security.authentication.TestingAuthenticationProvider\"/>\n");

    private ConfigTestUtilsEnum(String xml) {
        this.xml = xml;
    }
    private final String xml;
    @Override
    public String toString() {
        return this.xml;
    }
} 
