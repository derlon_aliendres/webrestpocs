/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250.config;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.access.event.AuthorizedEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

/**
 *
 * @author Derlon.Aliendres
 */
public class SessionRegistryApplicationListener/* extends SessionRegistryImpl */implements SessionRegistry, ApplicationListener<ApplicationEvent>{
    private final SessionRegistryImpl sessionRegistry;
    final protected Logger logger = Logger/*Factory*/.getLogger(getClass().getName());

    @Inject
    public SessionRegistryApplicationListener(SessionRegistryImpl sessionRegistry) {
        this.sessionRegistry = sessionRegistry;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof AuthorizedEvent) {
            System.out.println("EVENTO -> ContextRefreshedEvent");
//        } else if (event instanceof HttpSessionCreatedEvent) {
//            System.out.println("EVENTO -> HttpSessionCreatedEvent");
        } else if (event instanceof ContextRefreshedEvent) {
            System.out.println("EVENTO -> AuthorizedEvent");
//        } else if (event instanceof AuthenticationSuccessEvent) {
//            System.out.println("EVENTO -> AuthenticationSucessEvent");
//            final Authentication authentication = ( (AuthenticationSuccessEvent)event).getAuthentication();
//            logger.info("Autenticação registrada: " + authentication.getClass().getSimpleName() );
//            final UsernamePasswordWithTimeoutAuthenticationToken token = (UsernamePasswordWithTimeoutAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
//            logger.info("Autenticação registrada: " + token.getClass().getSimpleName() );
//            token.setAuthorities(new ArrayList<>(authentication.getAuthorities( ) ) );
//            event.
        } else if (event instanceof InteractiveAuthenticationSuccessEvent) {
            System.out.println("EVENTO -> InteractiveAuthenticationSucessEvent");
            try {
                final Authentication token = ( (InteractiveAuthenticationSuccessEvent)event).getAuthentication();
                 if (UsernamePasswordWithTimeoutAuthenticationToken.class.isInstance(token) ) {
                    UsernamePasswordWithTimeoutAuthenticationToken user = (UsernamePasswordWithTimeoutAuthenticationToken)token/*.getPrincipal()*/;
                     // aqui criamos um objeto pra armazenar o contexto do Spring. //
                     SecurityContext sc = SecurityContextHolder.getContext();
                    // agora armazeno o id da sessão atual através de um método da classe SessionRegistryUtils e da autenticação obtida do contexto. //
                    String idSessao = user.getDetails().getSessionId(); // SessionRegistryUtils.obtainSessionIdFromAuthentication(sc.getAuthentication())
                    sessionRegistry.registerNewSession(idSessao, user.getPrincipal())/*getName()*//*.get(0).expireNow()*/;
                     // depois pegamos um array com todas SessionInformation do usuário que acabou de logar. Usamos um método desta própria classe para isso. //
                     List<SessionInformation> sessoes = this.getAllSessions(user.getPrincipal(), true); // SessionRegistryUtils.obtainPrincipalFromAuthentication(sc.getAuthentication())
                     // essa iteração é feita para que possamos expirar as outras sessões que estiverem abertas. //
                     for (SessionInformation session: sessoes) {
                        int i = 1;
                        System.out.println("SessionInformation na Autenticação: " + i++ + "º: " + session.getSessionId() );
                     }
                 }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event instanceof SessionDestroyedEvent) {
            System.out.println("EVENTO -> HttpSessionDestroyedEvent");
            sessionRegistry.onApplicationEvent( (SessionDestroyedEvent)event);
        } else {
            System.out.println("EVENTO -> foi disparado um evento não tratado neste nível -> " + event.getClass());
        }
    }

    @Override
    public List<Object> getAllPrincipals() {
        return sessionRegistry.getAllPrincipals();
    }

    @Override
    public List<SessionInformation> getAllSessions(Object o, boolean bln) {
        return sessionRegistry.getAllSessions(o, bln); //
    }

    @Override
    public SessionInformation getSessionInformation(String string) {
        sessionRegistry.getSessionInformation(string); //
        return null;
    }

    @Override
    public void refreshLastRequest(String string) {
        sessionRegistry.refreshLastRequest(string); //
    }

    @Override
    public void registerNewSession(String string, Object o) {
        sessionRegistry.registerNewSession(string, o); //
    }

    @Override
    public void removeSessionInformation(String string) {
        sessionRegistry.removeSessionInformation(string); //
    }
    
}
