/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed; 
import javax.annotation.security.PermitAll; 
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
/**
 *
 * @author derlon.aliendres
 */
@PermitAll 
@Service //@Component 
public class Jsr250AnnotationDomainBizzService implements BusinessService /**/{

    @RolesAllowed("ROLE_USER") 
    @Override
    public void someUserMethod1() { 
        System.out.println("someUserMethod1(só printa SE O USUÁRIO TIVER O perfil 'ROLE_USER'!)");
    } 


    @RolesAllowed("ROLE_USER") 
    @Override
    public void someUserMethod2() { 
    } 


    @RolesAllowed({"ROLE_USER", "ROLE_ADMIN"}) 
    @Override
    public void someUserAndAdminMethod() { 
    } 


    @RolesAllowed("ROLE_ADMIN") 
    @Override
    public void someAdminMethod() { 
    } 


    @Override
    public int someOther(String input) { 
        return 0; 
    } 


    @Override
    public int someOther(int input) { 
        System.out.println("someOther(printa, pois o PRIVILÉGIO da classe é @PermitAll!)");
        return input; 
    } 


    @Override
    public List<?> methodReturningAList(List<?> someList) { 
        return someList; 
    } 


    @Override
    public List<?> methodReturningAList(String userName, String arg2) { 
        return new ArrayList<>(); 
    } 


    @Override
    public Object[] methodReturningAnArray(Object[] someArray) { 
        return null; 
    } 

    public void secureMethod() {
        System.out.println("secureMethod(só printa SE O USUÁRIO TIVER O PRIVILÉGIO!!!)");
    }
}
