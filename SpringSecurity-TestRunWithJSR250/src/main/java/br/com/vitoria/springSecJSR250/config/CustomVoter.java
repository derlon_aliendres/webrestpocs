/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250.config;

import br.com.vitoria.springSecJSR250.config.StandardSessionAuthenticationToken.SessionStandaloneInformation;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.event.AuthenticationFailureExpiredEvent;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Derlon.Aliendres
 */
public class CustomVoter implements AccessDecisionVoter<Object>/**/ {
    @Inject
    private SessionRegistry sessionRegistry;
    @Inject // 
    private ApplicationEventPublisher  _eventPublisher; 

    final protected Logger logger = Logger/*Factory*/.getLogger(getClass().getName());

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class/*<?>*/ clazzAuthentication) {
        return true/*org.aopalliance.intercept.MethodInvocation.class.isAssignableFrom(clazzAuthentication)*/;
    }

    @Override
    public int vote(Authentication authentication, Object object,/* ConfigAttributeDefinition*/Collection<ConfigAttribute> attributes) {
        logger.info("### Controle de Acesso  ###");

        //verifica se as credenciais são do tipo esperado
        if (SessionStandaloneInformation.class.isInstance(authentication.getDetails())) {/*authentication instanceof UsernamePasswordWithTimeoutAuthenticationToken*/
            Boolean result = null;
            UsernamePasswordWithTimeoutAuthenticationToken user = (UsernamePasswordWithTimeoutAuthenticationToken)authentication/*.getPrincipal()*/;
            final SessionStandaloneInformation sessnInfo = (SessionStandaloneInformation)(user.getDetails() );
            final String sessionId = sessnInfo.getSessionId();
            final Date datimeLastRequest = sessnInfo.getLastRequest();
            final long timeOutTime = extractNonceValue(user.getTimeout());

        //    for (ConfigAttribute configAttribute : attributes) {
        //        String attr = configAttribute.getAttribute();
                if ( ( (new Date().getTime() ) - datimeLastRequest.getTime() ) >  timeOutTime) { //attr.equals("ROLE_USER")
                    //ROLE_USER é a ROLE de usuário, logado, então retorna true sempre
                    result = false;
                } else {
                    //chama uma lógica específica que verifica se o usuário possui permissão no contexto atual
                    result = true; // gerenciadorPermissao.verificarPermissao(variavelSessao, attr)
                }
        //    }

            if (result == null || result == Boolean.FALSE) {
                logger.info(" -> Acesso Negado!");
                SecurityContextHolder.clearContext();/*.getContext().getAuthentication().setAuthenticated(false)*/; //authentication .clearContext()
                final AccountExpiredException userSessionExpiredException =
                        new AccountExpiredException("Sessão do Usuário expirou! Efetue o LogOn novamente.");
                _eventPublisher.publishEvent(new AuthenticationFailureExpiredEvent
                                                    (authentication, userSessionExpiredException) );
                throw userSessionExpiredException;
            //    return ACCESS_ABSTAIN; // ACCESS_DENIED
            } else {
                logger.info(" -> Acesso Permitido!");
                sessionRegistry.refreshLastRequest(sessionId);
                return ACCESS_GRANTED;
            }

        } else {
            System.out.println(" -> Não é do tipo UsernamePasswordWithTimeoutAuthenticationToken!");
            return ACCESS_ABSTAIN;
        }

    }

    private long extractNonceValue(String timeout) {
        return Long.valueOf/*getLong*/(timeout);   
    }

}