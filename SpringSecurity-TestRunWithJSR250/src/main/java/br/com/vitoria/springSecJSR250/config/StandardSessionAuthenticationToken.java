/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250.config;

import br.com.vitoria.springSecJSR250.config.StandardSessionAuthenticationToken.SessionStandaloneInformation;
import java.util.Collection;
import java.util.Date;
import javax.security.auth.Subject;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionIdentifierAware;
import org.springframework.security.core.session.SessionInformation;

/**
 *
 * @author derlon.aliendres
 */
public class StandardSessionAuthenticationToken/*<C extends SecurityContext, T extends SessionStandaloneInformation>*/
                                    extends UsernamePasswordAuthenticationToken implements AuthenticationDetailsSource {
//    private SessionStandaloneInformation details;
    protected Collection</*? extends */GrantedAuthority> authorities;

    public StandardSessionAuthenticationToken(Object principal, Object credentials, String sessionId, Date lastRequest) {
        this(principal, credentials, sessionId, lastRequest, null);
    }

    public StandardSessionAuthenticationToken(Object principal, Object credentials, String sessionId, Date lastRequest
                                              , Collection</*? extends */GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.setDetails(new SessionStandaloneInformation(principal, sessionId, lastRequest) );
        /*this.*/setAuthorities(authorities);
        this.setAuthorities(authorities);
    }

    /**
     *
     * @param context
     * @return
     */
    @Override
    public Object buildDetails(Object context) {
        final Object principal = ( (/*C*/SecurityContext)context).getAuthentication().getPrincipal(); //To change body of generated methods, choose Tools | Templates.
        this.setDetails(new SessionStandaloneInformation(principal, ( (SessionStandaloneInformation)getDetails() ).getSessionId(), new Date() ) );
        return this.getDetails();
    }

    @Override
    public boolean implies(Subject subject) {
        return super.implies(subject); //To change body of generated methods, choose Tools | Templates.
    }

    public static class SessionStandaloneInformation extends SessionInformation implements SessionIdentifierAware {

        public SessionStandaloneInformation(Object principal, String sessionId, Date lastRequest) {
            super(principal, sessionId, lastRequest);
        }
    }

    /**
     * @param authorities the authorities to set
     */
    public void setAuthorities(Collection</*? extends */GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    /**
     * @return the authorities
     */
    @Override
    public Collection</*? extends */GrantedAuthority> getAuthorities() {
        return  this.authorities;
    }

    /** 
     *(se estamos numa instância assigneble StandardSessionAuthenticationToken, obviamente o tipo de:
     * @return SessionStandaloneInformation
     */
    @Override
    public SessionStandaloneInformation getDetails() {
        return (SessionStandaloneInformation)super.getDetails();
    }

}
