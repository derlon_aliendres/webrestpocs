/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vitoria.springSecJSR250;

/**
 *
 * @author derlon.aliendres
 */
import java.util.List; 


import javax.annotation.security.RolesAllowed; 
import javax.annotation.security.PermitAll; 
import org.springframework.security.access.annotation.Secured;


//import org.springframework.security.access.annotation.Secured; 
import org.springframework.security.access.prepost.PreAuthorize; 


/** 
 */ 
@RolesAllowed/*Secured*/({"ROLE_USER"}) 
@PermitAll 
public interface BusinessService { 
    //~ Methods ======================================================================================================== 


    @Secured({"ROLE_ADMIN"}) //
    @RolesAllowed({"ROLE_ADMIN"}) 
    @PreAuthorize("hasRole('ROLE_ADMIN')") 
    public void someAdminMethod(); 


//    @Secured({"ROLE_USER", "ROLE_ADMIN"}) 
    @RolesAllowed({"ROLE_USER", "ROLE_ADMIN"}) 
    public void someUserAndAdminMethod(); 


//    @Secured({"ROLE_USER"}) 
    @RolesAllowed({"ROLE_USER"}) 
    public void someUserMethod1(); 


//    @Secured({"ROLE_USER"}) 
    @RolesAllowed({"ROLE_USER"}) 
    public void someUserMethod2(); 


    public int someOther(String s); 


    public int someOther(int input); 


    public List<?> methodReturningAList(List<?> someList); 


    public Object[] methodReturningAnArray(Object[] someArray); 


    public List<?> methodReturningAList(String userName, String extraParam); 


} 
