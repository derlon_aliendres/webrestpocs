/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise;

import br.com.mydomain.cdise.decorator.dip.MyService;
import com.mycompany.appmavenjpa.DepartmentRepo;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;

/**
 *
 * @author derlonaliendres
 */
public class MainApp {
    public static void main(String[] args) {

        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();

        // Starting the application-context enables use of @ApplicationScoped beans
        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContext(ApplicationScoped.class);

        // You can use CDI here
//        Developer developer = BeanProvider.getContextualReference(Developer.class, false);
//        developer.develop();
        MyService service = BeanProvider.getContextualReference(MyService.class, false);
        service.sayHello();

        DepartmentRepo departmentRepo = BeanProvider.getContextualReference(DepartmentRepo.class, false);
        departmentRepo.obterPelo(0L);
        cdiContainer.shutdown();
    }
}