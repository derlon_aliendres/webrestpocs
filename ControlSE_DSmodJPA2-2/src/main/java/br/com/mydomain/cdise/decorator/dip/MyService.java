package br.com.mydomain.cdise.decorator.dip;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author derlon.aliendres
 */
public interface MyService { 
  
 String sayHello();
 
  String metodoDelegationInvalido(String callerDecorator);
} 
