/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.Decorador;
import br.com.mydomain.cdise.decorator.dip.SecurityDecoratoravel;
import javax.enterprise.context.Dependent;
import javax.inject.Named;


@Named @Dependent @Decorador
public class SecurityDecoratorableImpl extends MyServiceImpl implements SecurityDecoratoravel {

    @Override
    public String metodoDelegationInvalido(String callerDecorator) {
        System.out.println("[ini]SecurityDecoratorableImpl.testMetodoDelegationInvalido: " + callerDecorator);
        return super.metodoDelegationInvalido(callerDecorator); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String sayHello() {
        System.out.println("[ini]SecurityDecoratorableImpl.sayHello: ");
        return super.sayHello(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
