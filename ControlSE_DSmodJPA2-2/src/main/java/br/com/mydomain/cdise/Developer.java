/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author Derlon.Aliendres
 */
@ApplicationScoped
public class Developer {
   @Inject
   private Coffee coffee;
   
    public String develop() {
       final String _developament = " developament!";
        System.out.println(_developament);
        return coffee.drink() + _developament;
    }
}
