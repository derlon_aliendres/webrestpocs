/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.MyService;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
//import org.springframework.stereotype.Component;

/**
 *
 * @author derlon.aliendres
 */
@Named @Dependent // @Component 
public class MyServiceImpl implements MyService {

    @Override
    public String sayHello() {
        System.out.println("[ini]ServiceImpl.sayHello()");
        return "Hello";
    }

    @Override
    public String metodoDelegationInvalido(String callerDecorator) {
        final String iniServiceImplInvld = "[ini]ServiceImpl.metodoInvalido:só executado se chamado p/mesmo método por:" + callerDecorator;
        System.out.println(iniServiceImplInvld);

        return callerDecorator;
    }

}
