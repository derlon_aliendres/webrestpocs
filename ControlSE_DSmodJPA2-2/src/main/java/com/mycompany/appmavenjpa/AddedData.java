/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.appmavenjpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;

/**
 *
 * @author RH
 */
public class AddedData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Vai criar EntityManagerFactory ...");
EntityManagerFactory entityManagerFactory = 
Persistence.createEntityManagerFactory("JPA2-2sePU"); //net.doraprojects_AppMaven_jar_5.2.6.FinalPU
       
        System.out.println("Vai criar EntityManager ...");
EntityManager entityManager = entityManagerFactory.createEntityManager();
        System.out.println("Vai criar tTransaction() ...");
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction/*.entityManager.getTransaction()*/.begin();

        System.out.println("Vai persistir d1 ...");
    Department d1 = new Department();
    d1.setName("Financial");
    entityManager.persist(d1);
    transaction.setRollbackOnly();
    entityManager.setFlushMode(FlushModeType.AUTO);
    entityManager.flush();//
transaction./*entityManager.getTransaction().*/rollback();
    
        final EntityTransaction transaction2 = entityManager.getTransaction();
        System.out.println("Vai persistir d2 ...");
    Department d2 = new Department();    
    d2.setName("Accountancy");
    transaction2.begin();
    transaction2.setRollbackOnly();
    entityManager.setFlushMode(FlushModeType.COMMIT);
    entityManager.persist(d2);
    d1 = entityManager.getReference(d1.getClass(), d1.getId() )/*.merge(d1)*/;
    entityManager.remove(d1);
    entityManager.flush();
 
    /*    entityManager.getTransaction()*/transaction2.rollback();//commit
        System.out.println(" commitou  ...");
entityManager.close();
        System.out.println("entityManager fechou  ...");
entityManagerFactory.close();
        System.out.println("entityManagerFactory fechou  [fim]");
    }
    
}
