/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.appmavenjpa;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import org.apache.deltaspike.jpa.api.entitymanager.PersistenceUnitName;

/**
 *
 * @author derlonaliendres
 */
@ApplicationScoped @Named 
public class EntityManagerProducer
{
    //or manual bootstrapping
    @PersistenceContext(unitName="JPA2-2sePU")
    private EntityManager entityManager;
    @PersistenceUnitName("JPA2-2sePU")
    private EntityManagerFactory entityManagerFactory;

    @PostConstruct
    public void init(EntityManagerFactory entityManager) {
        System.out.println("[EntityManagerProducer]init");
    //    this.entityManager = entityManager.createEntityManager();
    }
    @Produces
    @Dependent @Default
    protected EntityManager createEntityManager()
    {
        return this.getEntityManager();
    }

    protected void closeEntityManager(@Disposes EntityManager entityManager) {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    /**
     * @return the entityManagerFactory
     */
    @Produces
    @Dependent @Default
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private EntityManager getEntityManager() {
        if (entityManager == null) {
            EntityManagerFactory entityManagerFactory = 
            Persistence.createEntityManagerFactory("JPA2-2sePU"); // net.doraprojects_AppMaven_jar_5.2.6.FinalPU

            entityManager = entityManagerFactory.createEntityManager(); 
        }

        return entityManager;
    }
}