/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise;

import br.com.mydomain.cdise.Developer;
import javax.enterprise.inject.Instance;
//import javax.enterprise.inject.se.SeContainer;
//import javax.enterprise.inject.se.SeContainerInitializer;
import javax.inject.Inject;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;


/**
 *
 * @author Derlon.Aliendres
 */
@RunWith(CdiTestRunner.class)//
public class DeveloperTest {
    @Inject//
    public /*static */Developer developer;
    
    public DeveloperTest() {
    }
    
    @BeforeClass 
    public static void setUpClass() {
//        SeContainerInitializer initializer = SeContainerInitializer.newInstance(); //Weld weld = new Weld(); 
//        SeContainer initialize = initializer.initialize(); // WeldContainer initialize = weld.initialize()
//        Instance<Developer> lazyDeveloper = initialize.select(Developer.class);
//        developer = lazyDeveloper.get();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of develop method, of class Developer.
     */
    @Test
    public void testDevelop() {
        System.out.println("develop");
        assertNotNull(developer);
        String message = developer.develop();
        System.out.println("message = " + message);

        assertThat(message, org.hamcrest.CoreMatchers.containsString("Great") );
    }
    
}
