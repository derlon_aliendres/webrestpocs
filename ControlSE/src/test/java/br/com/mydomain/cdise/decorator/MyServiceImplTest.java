/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.Decorador;
import br.com.mydomain.cdise.decorator.dip.MyService;
import javax.inject.Inject;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import br.com.mydomain.cdise.decorator.dip.SecurityDecoratoravel;

/**
 *
 * @author Derlon.Aliendres
 */
@RunWith(CdiTestRunner.class)//
public class MyServiceImplTest {
 // This injected bean will be a decorated MyServiceImpl  
 @Inject // @Autowired 
 private MyService service; 
    
    public MyServiceImplTest() {
    }

 @Inject @Decorador// @Autowired 
 private /*MyServiceSecurityDecorator.*/SecurityDecoratoravel securityDecorator; 
  
 @Test 
 public void testSayHello() { 
     System.out.println("[ini]Decorator.testHelloWorld()");
     final String retReal = service.sayHello();
     /*Assert.*/assertTrue(retReal.contains("Hello"));
     System.out.println("[Finalize/volta]Decorator.testHelloWorld(): " + retReal);
 } 

    /**
     * Test of metodoDelegationInvalido method, of class MyServiceImpl.
     */
    @Test//(expected = IllegalStateException.class)
    public void testMetodoDelegationInvalido() {
        System.out.println("[ini]testMetodoDelegationInvalido");
        String callerDecorator = "ignorado";
        String expResult = callerDecorator; //""
        String result = securityDecorator.metodoDelegationInvalido(callerDecorator);
        assertEquals(expResult, result);//
     System.out.println("[Finalize/volta]Decorator.testHelloWorld(): " + result);
    }
} 
