/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 *
 * @author Derlon.Aliendres
 */
@Named @Dependent
public class Coffee {
    public String drink() {
        return "Great!";
    }
}
