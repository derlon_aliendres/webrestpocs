/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.Decorador;
import br.com.mydomain.cdise.decorator.dip.MyService;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import br.com.mydomain.cdise.decorator.dip.SecurityDecoratoravel;
import javax.annotation.Priority;
import static javax.interceptor.Interceptor.Priority.APPLICATION;

/**
 *
 * @author derlon.aliendres
 *//* */
@Decorator  @Priority(APPLICATION + 800)
 public /*abstract*/ class MyServiceSecurityDecorator implements /* SecurityDecoratoravel, */MyService { 
    @Inject
    @Decorador//@Delegate
    private SecurityDecoratoravel delegateSecurity;
    private final MyService delegate;

    @Inject
    public MyServiceSecurityDecorator(@Delegate MyService delegate) {
        this.delegate = delegate;
    }

    @Override
    public String sayHello() {
        System.out.println("[ini]DecoratorSec.sayHello()");
        String sec = "(securited)";
        return delegateSecurity/**/.sayHello() + sec;
    }

    @Override
    public String metodoDelegationInvalido(String callerDecorator) {
        System.out.println("[ini]DecoratorSec.metodoDelegationInvalido: chamando outro método:"
                            + callerDecorator);// do some transaction stuff 
        return delegateSecurity.sayHello();
    }
}
