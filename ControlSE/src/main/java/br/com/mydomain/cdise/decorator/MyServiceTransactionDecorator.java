/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.MyService;
import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import static javax.interceptor.Interceptor.Priority.APPLICATION;

/**
 *
 * @author derlon.aliendres
 */
@Decorator @Priority(APPLICATION + 10) // value = 
public abstract class MyServiceTransactionDecorator implements MyService {
    @Delegate @Inject
    private MyService delegate;

    @Override
    public String sayHello() {
        System.out.println("[ini]DecoratorTransact.sayHello()");// do some transaction stuff 
        String transact = "(transacted)";
        return delegate.sayHello() + transact;
    }

    @Override
    public String metodoDelegationInvalido(String callerDecorator) {
        System.out.println("[ini]DecoratorTransact.metodoDelegationInvalido: jogando fora o caller original(test):"
                            + callerDecorator);// do some transaction stuff 
        return delegate.metodoDelegationInvalido("DecoratorTransact");
    }

}
