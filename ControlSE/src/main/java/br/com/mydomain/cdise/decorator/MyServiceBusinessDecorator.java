/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mydomain.cdise.decorator;

import br.com.mydomain.cdise.decorator.dip.MyService;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import javax.annotation.Priority;
import static javax.interceptor.Interceptor.Priority.APPLICATION;

/**
 *
 * @author derlon.aliendres
 *//* */
@Decorator  @Priority(APPLICATION + 50)
 public /*abstract*/ class MyServiceBusinessDecorator implements MyService /**/{ 
    private final MyService delegate;

    @Inject
    public MyServiceBusinessDecorator(@Delegate
                                      MyService delegate) {
        this.delegate = delegate;
    }

    @Override
    public String sayHello() {
        System.out.println("[ini]DecoratorBizz.sayHello()");
        String sec = "(securited)";
        return delegate.sayHello() + sec;
    }

    @Override
    public String metodoDelegationInvalido(String callerDecorator) {
        System.out.println("[ini]DecoratorBizz.metodoDelegationInvalido: chamando outro método:"
                            + callerDecorator);// do some transaction stuff 
        return delegate/*Security*/.sayHello();
    }
}
