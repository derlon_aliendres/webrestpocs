/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.appmavenjpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

/**
 *
 * @author RH
 */
public class DepartmentRepo {
    public Department obterPelo(Long numNota) {
        final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Department> rootDepartment = cq.from(Department.class);
/*Join<Department,ItemDepartment> items = rootDepartment.joinCollection("itensDepartment", JoinType.LEFT);
        Metamodel m = em.getMetamodel();
        EntityType<Department> ItemDepartment_ = m.entity(Department.class);
        Fetch<Department,ItemDepartment> items = */
    /*    final Join<Object, Object> join = */rootDepartment.fetch("itensDepartment", JoinType.LEFT);
    //    join.on(cb.equal(rootDepartment.get("numNota"), "eu") );
        cq.select(rootDepartment).distinct(true)
                .where(cb.equal(rootDepartment.get("numNota"), numNota) ); //ItemDepartment_.numNota 
        Query qry = getEntityManager().createQuery(cq);
        List rslt = qry.getResultList();
        if (rslt.isEmpty() ) // .size() < 1
            throw new NoResultException("No entity found for query");
        return (Department)rslt.get(0); // qry.getSingleResult()
    }

    private EntityManager getEntityManager() {
EntityManagerFactory entityManagerFactory = 
Persistence.createEntityManagerFactory("com.mycompany_AppMaven_JPA2-2_jar_5.2.6.FinalPU"); // net.doraprojects_AppMaven_jar_5.2.6.FinalPU
       
EntityManager entityManager = entityManagerFactory.createEntityManager(); 
        return entityManager;
    }
    
}
