<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<c:set var="pathCntx" value="/"/>--%>
<c:url var="home" value="/"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
    <script src="${pageContext.request.contextPath}/recursos/JSs/jquery-1.9.1.min.js" type="text/javascript"></script><%-- http://jquerypriceformat.com/js/jquery-1.9.1.min.js http://code.jquery.com/jquery-1.7.2.min.js${pageContext.request.contextPath}/recursos/JSs/ --%>
<%--   <script src="${pageContext.request.contextPath}/recursos/JSs/jquery.maskedinput-1.3.min.js" type="text/javascript"></script><%-- http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/ --%>
   <script src="${pageContext.request.contextPath}/recursos/JSs/jquery.price_format.2.0.js"></script><%--  http://jquerypriceformat.com/js/jquery.price_format.2.0.js --%>
 <%--   <script src="${pageContext.request.contextPath}/recursos/JSs/ini.js"></script><%-- http://jquerypriceformat.com/js/ini.js
                <link rel="canonical" href="http://jquerypriceformat.com/" /> --%>
    
    <script type="text/javascript">
    	jQuery(function($){
	    //    $("#fone, #number").mask("9.999,99");
	    //    $('#preco').unmask();
            $('#preco').priceFormat({
	            prefix: 'R$ ',
	            centsSeparator: ',',
	            thousandsSeparator: '.'/*,
                clearPrefix: true */
	        }); /**/
	    });  
	    function retiraMaskDe(campoId) {
		    if (document.getElementById(campoId).value.indexOf('$') != -1) {
		       var unmask = $('#' + campoId).unmask(); // '#preco'
		       unmask = unmask / 100; // P/ tratamento: por onSubmit do form
		        document.getElementById(campoId).value = unmask; // P/ tratamento: por onSubmit do form
	    		console.log(unmask); // alert 123456 /* */
			}
	    }
    jQuery(document).ready(function() {
         $('a').click(function(event) {
          if (document.getElementById('idAjax').checked == true) {                        
             event.preventDefault();
             $.ajax({
                 url: $(this).attr('href') + '/AJAX', //'url'
                 dataType :'json',
             //    data : '{}',
                 success :  function(data){
                     console.log('Link via AJAX (JSON) com sucesso!!!');
                     console.log(JSON.stringify(data));// Your Code here alert(JSON.stringify(data) );
                     transToForm(data);
                 },             error : function(data) {
                                    url = $(this).attr('href');
                                    console.log(url);
                                    if (data.readyState = 404) {
                                        alert('N�o encontrado com o Id informado!');
                                    }
                                    console.log('An error occurred:' + data.responseText);
                                }
             })
           }
        });
        $('#idForm').submit(function(e) {
            retiraMaskDe('preco');
            if (document.getElementById('idAjax').checked == true) {                        
                        e.preventDefault(); // avoid to execute the actual submit of the form.
                        var aForm = jQuery(this); // $(this)
                        let formato = 'application/x-www-form-urlencoded', aceita = '';
                        var json = (metodo == 'POST'? aForm.serialize(): JSON.stringify(convertvRaptorFormToJSON("#idForm")) ); // 
                        var contntFormat = (metodo != 'POST'? 'application/json;charset=UTF-8': ''); // 
                        if (metodo != 'POST') {
                            aceita = contntFormat;
                           formato = contntFormat;
                        }
                        var idf = aForm.children('#produto_id').val(); //$('#produto_id').val() document.getElementById('produto.id').value 
                //    alert('idf ' + idf + ' '/* + aForm.serialize() */);//    $('#acao').val('AJAX');
                        $.ajax({
                               type : metodo, //'PUT' aForm.attr('method') 'POST';charset=utf-8
                               contentType: formato,// NOT dataType!  <<<<<-- Enviar no aFormato JSON
                               Accept: formato, //"application/json" ; charset=utf-8    application/x-www-form-urlencoded   
                               url : aForm.attr('action') /*'http://localhost:8084' +  + (metodo == 'PUT'? '/AJAX': '') */,
                               data: json, /*{"produto.id": idf}produto: {}, acao: 'AJAX'aForm.serialize()'test' }*/// serializes the form's elements.
                               dataType: 'json',//
                            //   context : aForm,
                               success : function(data) {
                               //     $('#idReset').click();// $('#acao').val('AJAX');   form.reset();
                                    if (metodo == 'POST') {
                                        metodo = 'incluido';
                                        addLinhaNa('table', data.produto.id, data.produto.nome, data.produto.descricao, data.produto.preco);
                                    } else if (metodo == 'DELETE') {
                                        metodo = 'excluido';
                                        removerLinhaNa('table', data.produto.id);
                                    } else {
                                        console.log('running PUT!!!');
                                        metodo = 'alterado';
                                        updateLinhaNa('table', data.produto.id, data.produto.nome, data.produto.descricao, data.produto.preco);
                                    }

                                //    $('#idAjax').trigger('click');
                                   alert('Produto ' + metodo + ' com sucesso!'); // show response from the php script.
                                   resetForm();
                                },
                                error : function(data) {
                                    url = aForm.attr('action');
                                    console.log(url);
                                    alert(data.responseText);
                                    console.log('An error occurred.');
                                    console.log(data.responseText);
                                },
                             });
                    }
        });
    });
 
    function convertFormToJSON(idForm){
        console.log('ConvertFormToJSON invoked!');
        var array = jQuery(idForm).serializeArray();
        var json = {};

        jQuery.each(array, function() {
            json[this.name] = this.value || '';
        });

        console.log('JSON: '+json);
        return json;
    }
 
    function convertvRaptorFormToJSON(idForm){
        console.log('ConvertvRaptorFormToJSON invoked!');
        var array = jQuery(idForm).serializeArray();
        var json = {};

        jQuery.each(array, function() {
            let paraName = this.name;
            if (paraName.indexOf('.') > 1) {
                let vet = paraName.split('.');
                let entidade = vet[0];
                let atributo = vet[1];
                if (!json[entidade]) {
                    json[entidade] = {};
                }
                json[entidade][atributo] = this.value || '';
            } else {
                json[paraName] = this.value || '';
            }
        });
        console.log('JSON: ' + JSON.stringify(json) );

        return json;
    }
 
    function transToForm(data) {
        var arrayFields = ['id', 'nome', 'descricao']; // , 'preco'
        jQuery.each(arrayFields, function() {console.log(': ' + this + ': ' + data.produto[this] );
            document.getElementById('produto.' + this).value = data.produto[this];
        }); document.getElementById('preco').value = data.produto['preco'];
        tratarBtns(document.getElementById('produto.id').value)
    }
 
    function addLinhaNa(tableId, col1, col2, col3, col4) {
        var newRow = $('<tr id="' + col1 + '">');
        var cols = '';

        cols += '<td><a href="${pathCntx}' + col1 + '">' + col1 + '</a> ' + col1 + '</td> ';
        cols += '<td>' + col2 + '</td>';
        cols += '<td>' + col3 + '</td>';
        cols += '<td style="text-align: right">' + round2dig(col4) + '</td>';
     //   cols += '<td>'; cols += '<button onclick="RemoveTableRow(this)" type="button">Remover</button>'; cols += '</td>';

        newRow.append(cols);
        $(tableId).append(newRow);
    }

    function removerLinhaNa(tableId, col1) {
        var par = $('#' + col1); //tr $(this).parent().parent()
        par.remove();
      //  $(tableId).append(newRow);
    }
 
    function updateLinhaNa(tableId, col1, col2, col3, col4) {
        var newRow = $('#' + col1);
        var cols = '';

        newRow.children("td:nth-child(4)").remove();
        newRow.children("td:nth-child(3)").remove();//newRow.html('')
        newRow.children("td:nth-child(2)").remove();//cols += '<td><a href="${pathCntx}' + col1 + '">' + col1 + '</a> ' + col1 + '</td> ';
        cols += '<td>' + col2 + '</td>';
        cols += '<td>' + col3 + '</td>';
        cols += '<td style="text-align: right">' + round2dig(col4) + '</td>';
    //    cols += '<td>' cols += '<button onclick="RemoveTableRow(this)" type="button">Remover</button>'; cols += '</td>';

        newRow.append(cols);
    //    $(tableId).append(newRow);
    }
     </script>      
   </head>
<body>
	<table id="table" border="1">
            <thead>
            <th>Cod.</th><th>Nome</th><th>Descri��o</th><th>Valor</th>
            </thead>
	  <c:forEach items="${produtoList}" var="produto">
            <tr id="${produto.id}">
		  	<td><a href="${pathCntx}${produto.id}">${produto.id}</a>
				${produto.id} </td>
		  	<td> ${produto.nome} </td>
		  	<td> ${produto.descricao} </td>
		  	<td style="text-align: right"><fmt:formatNumber value = "${produto.preco}" type = "currency"/>  </td>
            </tr>
	  </c:forEach>
	</table>                               <%--<c:url value=""/> s/incluir --%>
    <form id="idForm" action="${pageContext.request.contextPath}/produto" method="post" onsubmit="" >
            <input type="radio" id="full" name="acceptType" value="full" checked />
            <label for="full">Full</label>
            <input type="radio" id="idAjax" name="acceptType" value="AJAX" />
            <label for="idAjax">AJAX</label><br />
            Nome: <input type="text" id="produto.nome" name="produto.nome" value="${produto.nome}" /><br/>
            Descri��o: <input type="text" id="produto.descricao" name="produto.descricao" value="${produto.descricao}" /><br/>
	  Pre�o: <input type="text" id="preco" name="produto.preco" value="${produto.preco}" <%--value="123456"--%> /><br/>
	 <%-- Telefone: <input type="text" id="fone" name="produtXYZ" /><br/>--%>
          <input type="text" id="produto.id" name="produto.id"  value="${produto.id}" readonly /><%--   type="hidden" --%>
          <button type="submit" id="incluir" name="_method" value="POST" ${(not empty produto.id? 'disabled': '')}>Cadastrar</button>
            <button type="submit" id="excluir" name="_method" value="DELETE" ${(empty produto.id? 'disabled': '')}>remover produto</button>
            <button type="submit" id="alterar" name="_method" value="PUT" ${(empty produto.id? 'disabled': '')}>Alterar</button>
            <input type="button" id="idReset" value="Limpar" />
	  <input type="submit" value="Salvar" />  <input type="button" value="Salvar" onclick="alert(document.getElementById('preco').value)" />
          <input type="checkbox" id="idChckBx" />AjaxServlet
          <dir id="msgs">
            <span style="color: red">${msgErro}</span>
            <span style="color: blue">${msgSucesso}</span>
          </dir>
	</form>
<footer>
<script type="text/javascript">
    function tratarBtns(id) {
        if (id) {
            document.getElementById('incluir').setAttribute('disabled', 'disabled');
            document.getElementById('excluir').removeAttribute('disabled');
            document.getElementById('alterar').removeAttribute('disabled');
         } else {
            document.getElementById('incluir').removeAttribute('disabled');
            document.getElementById('excluir').setAttribute('disabled', 'disabled');
            document.getElementById('alterar').setAttribute('disabled', 'disabled');
         }
    }
    function resetForm() {
        clearValoreDe('input[type=text]');
        tratarBtns(document.getElementById('produto.id').value)
    }
    function clearValoreDe(selector) {
        $(selector).val('');
    }
    jQuery(document).ready(function() {
//        $('#idForm').submit(function(e) {
//            retiraMaskDe('preco');
//            submiter(e);
//        });
        $('button').click(function(){
          metodo = $(this).val();
        //  var nome = $(this).attr('name');
        });
        $('#idReset').click(function(){
          resetForm();
       //  var nome = $(this).attr('name');
       });
       $('#idChckBx').change(function() {
          if ($(this).is(':checked') ) {
           $('#idForm').attr('action', '${pageContext.request.contextPath}/AjaxServlet' );
            } else {
                var vRaptor = '${home}produto';
                console.log('vRaptor: ' + vRaptor)
                $('#idForm').attr('action', vRaptor);
            }
        });/**/
    });
</script>
<script language="JavaScript">	var Casas;//	var Num = prompt("Digite um n�mero com virgula logo!","");
//Num = dec2casas(Casas, Num);
function dec2casas(Casas, numb) {
    var Busca, Num2; numb = numb.replace('.', ',');
    Busca = numb.indexOf(",", 0);
    if (Busca == -1){
        numb = numb + "," + 0 + 0;
    }else if (Busca == numb.length-2){
    /*    Casas = numb.substring(Number(Busca+1), Number(Busca+3));
        Num2 = numb.substring(0, Number(Busca+1));
        numb = Num2 + Casas;*/
        numb = numb + 0;
    }
    return numb;
}
//alert(Num);//
function arredondar(str, casas) {
  /*  if (str.indexOf(',') != -1)*/ str = str.replace(',', '.');
    if (!casas) casas = 0;
    casas = Math.pow(10, casas);
    str = parseFloat(str) * casas;
    return Math.floor(str) / casas;
}
//var rnd = arredondar(Num, 2); // '100'
//alert(rnd);
//alert(dec2casas('', rnd.toString() ) );

function round2dig(numero) {
    if (typeof numero !== 'number') { // )
        numero = numero.replace(',', '.');
    } //  var rnd = arredondar(numero.toString(), 2);
    return Number.parseFloat(numero).toFixed(2).replace('.', ',') ; // dec2casas('', rnd.toString() );
}
//alert(round2dig(Num) );//
</Script>
</footer>
</body>
</html>