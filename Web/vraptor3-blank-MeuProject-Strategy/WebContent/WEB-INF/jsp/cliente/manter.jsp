<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insert title here</title>
    <script src="${pageContext.request.contextPath}/recursos/JSs/jquery-1.9.1.min.js" type="text/javascript"></script>
<%--    <script src="${pageContext.request.contextPath}/recursos/JSs/jQuery-1.8.2/jquery.js" type="text/javascript"></script>
     http://jquerypriceformat.com/js/jquery-1.9.1.min.js http://code.jquery.com/jquery-1.7.2.min.js${pageContext.request.contextPath}/recursos/JSs/ --%>
<%--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"></script>
     <script src="${pageContext.request.contextPath}/recursos/JSs/jquery-1.9.1.min.js" type="text/javascript"></script>
<%- - http://jquerypriceformat.com/js/jquery-1.9.1.min.js http://code.jquery.com/jquery-1.7.2.min.js${pageContext.request.contextPath}/recursos/JSs/ --%>
<!--script src="${pageContext.request.contextPath}/recursos/JSs/jquery-1.9.1.min.js" type="text/javascript"></script>-->
    <%-- http://jquerypriceformat.com/js/jquery-1.9.1.min.js ${pageContext.request.contextPath}/recursos/JSs/ --%>
   <script src="${pageContext.request.contextPath}/recursos/JSs/jquery.maskedinput-1.3.min.js" type="text/javascript"></script><%--  - -%>
   <script src="${pageContext.request.contextPath}/recursos/JSs/jquery.price_format.2.0.js"></script> http://jquerypriceformat.com/js/jquery.price_format.2.0.js --%>
   <script src="${pageContext.request.contextPath}/recursos/JSs/utilJSON.js" type="text/javascript"></script>
            <script type="text/javascript">
  var metodo;
      
  $(document).ready(function() {
        $('#idForm').submit(function(e) {
                    $('#imgStatusWait').show();
                    if (document.getElementById('idAjax').checked == true) {                        
                        e.preventDefault(); // avoid to execute the actual submit of the form.
                        var form = $(this);
                        var json = ( (metodo == 'POST' || metodo == 'GET')? form.serialize(): JSON.stringify(convertvRaptorFormToJSON("#idForm")) ); // 
                        var idf = form.children('#cliente.id').val(); //$('#cliente.id').val() document.getElementById('cliente.id').value 
                //    alert('idf ' + idf);//    $('#acao').val('AJAX');
                        $.ajax({
                               type: metodo, //'PUT' form.attr('method') 'POST'
                               contentType: ( (metodo == 'POST' || metodo == 'GET')? 'application/x-www-form-urlencoded': 'application/json;charset=UTF-8'), // NOT dataType!  <<<<<-- Enviar no formato JSON
                               url: form.attr('action')/* + (metodo == 'PUT'? '/AJAX': '')  + '/AJAX'*/,
                               data:/* JSON.stringify(*/json/*)*//*form.serialize(){"cliente.id": idf}'test' , acao: }*/, // serializes the form's elements.
                               dataType: 'json',
                               statusCode: {
                                    404: function(request , status, error) {
                                       alert( "page not found: " + error);
                                        console.log('An error occurred: ' + status);
                                        console.log('error: ' + error); console.log(request.responseText);
                                    }
                                }
                               }//
                              ).done(function(data) {
                               //     $('#idReset').click();// $('#acao').val('AJAX');   form.reset();
                                    if (metodo == 'POST') {
                                        metodo = 'incluido';
                                        addLinhaNa('table', data.cliente.id, data.cliente.nome, data.cliente.login);
                                    } else if (metodo == 'DELETE') {
                                        metodo = 'excluido';
                                        removerLinhaNa('table', data.cliente.id);
                                    } else if (metodo == 'GET') {
                                        metodo = 'Pesq';
                                        updateA('table', data);
                                    } else {
                                        console.log('running PUT!!!');
                                        metodo = 'alterado';
                                        updateLinhaNa('table', data.cliente.id, data.cliente.nome, data.cliente.login/*, data.cliente.preco*/);
                                    }
                                //    $('#idAjax').trigger('click');
                                   alert('Cliente ' + metodo + ' com sucesso!'); // show response from the php script.
                                   $('#msgs').hide();
                                   resetForm();
                                }
                              )/* catch */.fail(function (response, status, error) {
                                    url = form.attr('action');
                                    console.log(url);
                                    let msgWarn = (response.status === 500? error: JSON.parse(response.responseText).string);
                                    alert(msgWarn);
                                    console.log('An error occurred: ' + status);
                                    console.log('error: ' + error);
                                    console.log(msgWarn);
                                }
                              ).always(function(data) {
                                    console.log("SEMPRE FUNFA!"); 
                                    //A function to be called when the request finishes 
                                    // (after success and error callbacks are executed). 
                                    $('#imgStatusWait').hide();
                                }
                              );
                    }
        });
        applyNosLinksChamadaAJAX();
  });
 
    function transToForm(data) {
        var arrayFields = ['id', 'nome', 'login']; // , 'preco'
        jQuery.each(arrayFields, function() {console.log(': ' + this + ': ' + data.cliente[this] );
            document.getElementById('cliente.' + this).value = data.cliente[this];
        });// document.getElementById('preco').value = data.cliente['preco'];
        tratarBtns(document.getElementById('cliente.id').value)
    }

    function applyNosLinksChamadaAJAX() {
        $('a').click(function(event) {
          if (document.getElementById('idAjax').checked === true && !$(this).attr('href').includes('lista') ) {                        
             event.preventDefault();
             $('#imgStatusWait').show();
             const URI = $(this).attr('href') + '/AJAX';
             console.log('URI no link:' + URI);
             $.ajax({
                 url: URI, //'url'
                 dataType :'json',
             //    data : '{}',
                 success :  function(data){
                     console.log('Link via AJAX (JSON) com sucesso!!!');
                     console.log(JSON.stringify(data));// Your Code here alert(JSON.stringify(data) );
                     transToForm(data);
                 },             error : function(data) {
                                    url = $(this).attr('href');
                                    console.log(url);
                                    if (data.readyState = 404) {
                                        alert('N�o encontrado com o Id informado!');
                                    }
                                    console.log('An error occurred:' + data.responseText);
                                }
             }).always(function(data) { console.log("finally do applyNosLinks!"); 
                                        $('#imgStatusWait').hide();
                                        $('#msgs').hide();
                                      }
              )
           }
        });
    }

    function addLinhaNa(tableId, col1, col2, col3) {
        var newRow = $('<tr id="' + col1 + '">');
        var cols = '';

        cols += '<td><a href="${pathCntx}' + col1 + '">' + col1 + '</a> ' + col1 + '</td> ';
        cols += '<td>' + col2 + '</td>';
        cols += '<td>' + col3 + '</td>';
        cols += '<td>';
      //  cols += '<button onclick="RemoveTableRow(this)" type="button">Remover</button>';
        cols += '</td>';

        newRow.append(cols);
        $(tableId).append(newRow);
        applyNosLinksChamadaAJAX();
}
 
    function updateLinhaNa(tableId, col1, col2, col3, col4) {
        var newRow = $('#' + col1);
        var cols = '';

        newRow.children("td:nth-child(4)").remove();
        newRow.children("td:nth-child(3)").remove();//newRow.html('')
        newRow.children("td:nth-child(2)").remove();//cols += '<td><a href="${pathCntx}' + col1 + '">' + col1 + '</a> ' + col1 + '</td> ';
        cols += '<td>' + col2 + '</td>';
        cols += '<td>' + col3 + '</td>';
        cols += '<td style="text-align: right">' +/* round2dig(* /col4/*) + */'</td>';
    //    cols += '<td>' cols += '<button onclick="RemoveTableRow(this)" type="button">Remover</button>'; cols += '</td>';

        newRow.append(cols);
    //    $(tableId).append(newRow);
    }

    function updateA(tableId, lstJSON) {
        let entityIDs = [];
        let trIDs = [];
        const lstEnts = lstJSON.list/**/;
        jQuery.each(lstEnts, function() {
            entityIDs.push(this.id);
        });
        $('#' + tableId + ' tr[id]').each(function() {
            let rtId = parseInt($(this).attr('id') );
            console.log('id : ' + rtId);
            trIDs.push(rtId);
          if (entityIDs.includes(rtId) ) {
            const client = lstEnts[entityIDs.indexOf(rtId)]
            updateLinhaNa('table', client.id, client.nome, client.login/*, client.preco*/);
          } else {
            removerLinhaNa('table', rtId);
          }
        });
        jQuery.each(lstEnts, function() {
          if (!trIDs.includes(this.id) ) {
            insereLinhaNa('table', this.id, this.nome, this.login/*, this.preco*/);
          }
        });
    }

    function insereLinhaNa(tableId, col1, col2, col3) {
        var newRow = $('<tr id="' + col1 + '">');
        var linhas = [];
//        var cols = '';
//
//        cols += '<td><a href="' + col1 + '">' + col1 + '</a> ' + col1 + '</td> ';
//        cols += '<td>' + col2 + '</td>';
//        cols += '<td>' + col3 + '</td>';
//        cols += '<td>';
//      //  cols += '<button onclick="RemoveTableRow(this)" type="button">Remover</button>';
//        cols += '</td>';
        $('#' + tableId + ' tr[id]').each(function() {
            let rtId = parseInt($(this).attr('id') );
            console.log('id : ' + rtId);
          if ( (1 * col1) < (1 * rtId) ) {
            linhas.push($(this));
            $(this).remove(); // removerLinhaNa('table', rtId);
          //  updateLinhaNa('table', client.id, client.nome, client.login/*, client.preco*/);
          } else {
            //Don't do anything!!;
          }
        });
//        newRow.append(cols);
        addLinhaNa('table', col1, col2, col3);// $(tableId).append(newRow);
        jQuery.each(linhas, function() {
          $(tableId).append($(this) );
        });
        applyNosLinksChamadaAJAX();
    }
        </script>
    </head>
<body>
	<a href="lista">cliente/lista</a><br />
        <table id="table"  border="1">
            <thead>
              <tr>
                <th></th><th>Nome</th><th>Senha</th><th>Idade</th>
              </tr>
            </thead>
            <c:forEach var="cliente" items="${clienteList}">
              <tr id="${cliente.id}">
                <td><!-- cliente/ /full-->
                    <a href="${pathCntx}${cliente.id}">${pathCntx}${cliente.id}</a>
                      ${cliente.id} 
                </td>
                <td>${cliente.nome}</td>
                <td>${cliente.login}</td>
                <%--  	<td> ${cliente.idade} </td>--%>
              </tr>
            </c:forEach>
        </table><c:url var="home" value="/"/>
    <div id="imgStatusWait" style="display: none; width: 20%; text-align: center">
        <img id="imgStatusWait" src="${home}recursos/imagens/loading_sgf_campo.gif"/></div><%-- ${cont} ${pageContext.request.contextPath}/ --%>
    <form id="idForm" method="post" action="${home}cliente">
    <%--    <input type="hidden" id="acao" name="acao" />tipo --%>
             <input type="radio" id="full" name="acceptType" value="full" checked />
             <label for="full" alt="Full Submit" title="Full Submit">Full</label>
            <input type="radio" id="idAjax" name="acceptType" value="AJAX" />
            <label for="idAjax">AJAX</label><br />
            Nome: <input type="text" id="cliente.nome" name="cliente.nome" value="${cliente.nome}" /><br/>
	  login: <input type="text" id="cliente.login" name="cliente.login" value="${cliente.login}" /><br/>
          <%--      Idade: <input type="text" name="cliente.idade" disabled /><br/>--%>
          <input type="text" id="cliente.id" name="cliente.id"  value="${cliente.id}" readonly /><%--   type="hidden" --%>
          <button type="submit" id="pesquisar" name="_method" value="GET" >Pesquisar</button><br />
            <button type="submit" id="incluir" name="_method" value="POST" ${(not empty cliente.id? 'disabled': '')}>Cadastrar</button>
            <button type="submit" id="excluir" name="_method" value="DELETE" ${(empty cliente.id? 'disabled': '')}>remover cliente</button>
            <button type="submit" id="alterar" name="_method" value="PUT" ${(empty cliente.id? 'disabled': '')}>Alterar</button>
            <input type="button" id="idReset" value="Limpar" />
            <input type="checkbox" id="idChckBx" />AjaxServlet
          <div id="msgs" style="display: ${(empty msgErro and empty msgSucesso)? 'none': 'block'}">
            <span id="msgErro" style="color: red">${msgErro}</span>
            <span id="msgSucesso" style="color: blue">${msgSucesso}</span>
          </div>
	</form>
                <script type="text/javascript">
    function tratarBtns(id) {
        if (id) {
            document.getElementById('incluir').setAttribute('disabled', 'disabled');
            document.getElementById('excluir').removeAttribute('disabled');
            document.getElementById('alterar').removeAttribute('disabled');
         } else {
            document.getElementById('incluir').removeAttribute('disabled');
            document.getElementById('excluir').setAttribute('disabled', 'disabled');
            document.getElementById('alterar').setAttribute('disabled', 'disabled');
         }
    }

    function resetForm() {
        clearValoreDe('input[type=text]');
        document.getElementById('cliente.id').focus();
        tratarBtns(document.getElementById('cliente.id').value);
        $("#cliente.id").blur(); //document.getElementById('cliente.id').l
    }

    function clearValoreDe(selector) {
        $(selector).val('');
    }
                    
$(document).ready(function() {
    $('button').click(function(){
      metodo = $(this).val();
    //  var nome = $(this).attr('name');
    });
        $('#idReset').click(function(){
          $('text').val('');
        //  var nome = $(this).attr('name');
        });
        $('#idReset').click(function(){
          resetForm();
       //  var nome = $(this).attr('name');
       });
    $('#idChckBx').change(function() {
      if ($(this).is(':checked') ) {
       $('#idForm').attr('action', '/AjaxServlet' );
        } else {
            var vRaptor = '${home}cliente';
            console.log('vRaptor: ' + vRaptor)
            $('#idForm').attr('action', vRaptor);
        }
    //  var nome = $(this).attr('name');
    });
    document.getElementById('cliente.id').addEventListener('change', function() {
        document.getElementById('cliente.id').focus();
                                                                                    alert('ok: ' + this.value) 
    //    $("#cliente.id").blur(); //document.getElementById('cliente.id').l
                                                                                }
                                                            );
       $('#cliente_id').click(function() {
          tratarBtns($(this).val() );
        });
});
</script>
<footer>
</footer>
</body>
</html>