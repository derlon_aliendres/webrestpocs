/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    function convertFormToJSON(idForm){
        console.log('ConvertFormToJSON invoked!');
        var array = jQuery(idForm).serializeArray();
        var json = {};

        jQuery.each(array, function() {
            json[this.name] = this.value || '';
        });

        console.log('JSON: '+json);
        return json;
    }
    //** Obtem dados do (input name) estilo vRaptor */
    function convertvRaptorFormToJSON(idForm){
        console.log('ConvertvRaptorFormToJSON invoked!');
        var array = jQuery(idForm).serializeArray();
        var json = {};

        jQuery.each(array, function() {
            let paraName = this.name;
            if (paraName.indexOf('.') > 1) {
                let vet = paraName.split('.');
                let entidade = vet[0];
                let atributo = vet[1];
                if (!json[entidade]) {
                    json[entidade] = {};
                }
                json[entidade][atributo] = this.value || '';
            } else {
                json[paraName] = this.value || '';
            }
        });
        console.log('JSON: ' + JSON.stringify(json) );

        return json;
    }
 

    function removerLinhaNa(tableId, col1) {
        var par = $('#' + col1); //tr $(this).parent().parent()
        par.remove();
      //  $(tableId).append(newRow);
    }

