<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="<c:url value="/" />">home</a><br />
	<h1>Obter Licenca</h1>
    <c:if test="${not empty msgErro}">  
      <c:out value="${msgErro}"/>  
    </c:if>
    <c:if test="${not empty msgSucesso}">  
      <c:out value="${msgSucesso}"/>  
    </c:if>
  	<table border="1">
	  	<tr>
		  	<td>Lista</td>
		</tr>
	  <c:forEach items="${votosList}" var="voto">
	  	<tr>
		  	<td>
		  		<a href="<c:url value="/direito/criarLicenca?id=${cidadao.id}"/>">${cidadao.id}</a>
				${cidadao.id} 
		  	</td>
		  	<td> ${cidadao.nome} </td>
		  	<td> ${cidadao.senha} </td>
		  	<td> ${cidadao.idade} </td>
		</tr>
	  </c:forEach>
	</table>
</body>
</html>