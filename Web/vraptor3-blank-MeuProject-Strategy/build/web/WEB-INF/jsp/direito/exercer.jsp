<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Exercer Direito</h1>
	<form method="POST" action="<c:url value="/direito/exercer" />">
	  Nome: <input type="text" name="cidadao.nome" /><br/>
	  Idade: <input type="text" name="cidadao.idade" /><br/>
	  <input type="radio" name="tipoDireito" title="Votar" value="0"/>Votar
	  <input type="radio" name="tipoDireito" title="Dirigir" value="1"/>Dirigir<br />
	  <!-- button type="submit" name="_method" value="POST">Exercer Direito</button--><br />
	  <input type="submit" value="Exercer Direito" /><br />
	</form>
    <c:if test="${not empty msgErro}">  
      <c:out value="${msgErro}"/>  
    </c:if>
    <c:if test="${not empty msgSucesso}">  
      <c:out value="${msgSucesso}"/>  
    </c:if>
  	<table border="1">
	  	<tr>
		  	<td>Lista</td>
		</tr>
	  <c:forEach items="${votosList}" var="voto">
	  	<tr>
		  	<td> ${voto} </td>
		</tr>
	  </c:forEach>
	</table>
</body>
</html>