<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="<c:url value="/" />">home</a><br />
	<h1>Criar Licenca</h1>
	<form method="POST" action="<c:url value="/direito/criarLicenca" />">
	  Nome: <input type="text" name="cidadao.nome" value="${cidadao.nome}" readonly="readonly" /><!-- disabled="disabled" --><br/>
	  Idade: <input type="text" name="cidadao.idade" value="${cidadao.idade}" readonly="readonly" /><br/>
	  <input type="hidden" name="cidadao.id" value="${cidadao.id}" />
	  <c:forEach items="${EnumDireitoSpec}" var="ED">
	  	<input type="radio" name="enumDireitoSpec" title="${ED.nomeDireito}"
	  			 value="${ED}"/>
	  	${ED}
	  	 ${ED.nomeDireito}
	  </c:forEach>
	  <!-- button type="submit" name="_method" value="POST">Exercer Direito</button--><br />
	  <input type="submit" value="Gerar Registro" /><br />
	</form>
    <c:if test="${not empty msgErro}">  
      <c:out value="${msgErro}"/>  
    </c:if>
    <c:if test="${not empty msgSucesso}">  
      <c:out value="${msgSucesso}"/>  
    </c:if>
	<table border="1">
	  	<tr>
		  	<td>Habilit.</td><td>Nome</td><td>Id</td><td>Idade</td>
		</tr>
	  <c:forEach items="${direitosDoCidList}" var="direito">
	  	<tr>
		  	<td>
		  		<a href="<c:url value="/direito/participarEvento?id=${direito.id}"/>">${direito.id}</a><!-- .cidadao -->
				${direito.descricao} 
		  	</td>
		  	<td> ${direito.cidadao.nome} </td>
		  	<td> ${direito.cidadao.id} </td>
		  	<td> ${direito.cidadao.idade} </td>
		</tr>
	  </c:forEach>
	</table>
</body>
</html>