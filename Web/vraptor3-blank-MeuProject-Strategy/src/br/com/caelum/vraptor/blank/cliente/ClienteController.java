package br.com.caelum.vraptor.blank.cliente;

import appServidora.domain.model.Cliente;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import appServidora.domain.service.ClienteDomSvc;
import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import static br.com.caelum.vraptor.view.Results.json;

@Resource
public class ClienteController {

    @Autowired
    private  Result result; // final

    private ClienteDomSvc clienteDomSvc;

    public ClienteController(ClienteDomSvc clienteDomSvc) { //, Result result, Validator validator,
          this.clienteDomSvc = clienteDomSvc;
    }

    public List<Cliente> lista() {
          System.out.println("Retornando a List para a View!!");
          return clienteDomSvc.listaTodos(); //result.include("clientes", );
    }

    public void adiciona(Cliente cliente/*, String produtXYZ*/) {
    //      System.out.println("Param: " + produtXYZ);  
          clienteDomSvc.adiciona(cliente);  //
          result.redirectTo(ClienteController.class).lista();
    }

    public void cadastrar() {
          System.out.println("Entrou em 'cadastrar()'!!");  
    }

//  @Path({"/cliente/lista"}) // /{acao}
    public List<Cliente> manter(Cliente cliente) {
        System.out.println("Entrou em 'manter()'!!");  
        return clienteDomSvc.pesquisarPor(cliente ); //.listaTodos()  result.include("clientes", );
    }

  @Get
  @Path({"/cliente/{cliente.id}", "/cliente/{cliente.id}/{acao}"}) // /{acao}
  public/* Cliente */void visualiza(Cliente cliente, String acao) {
    try {
      cliente =  clienteDomSvc.obterPeloId(cliente/*.getId() */);
        if ("AJAX".equalsIgnoreCase(acao) ) {
            result.use(Results.representation()/*json() */).from(cliente).serialize();
        }
        result.include("cliente", cliente); // 
    } catch (Exception e) {
        final String msgFalha = "Cliente não encontrado com id informado!" ;
        if ("AJAX".equalsIgnoreCase(acao) ) {
            result.use(Results.http() ).body(msgFalha).sendError(404, msgFalha); // 
        } else {
            result.include("msgErro", msgFalha );
        }
    }
    if (!"AJAX".equalsIgnoreCase(acao) ) {
        result.redirectTo(ClienteController.class).manter(null);
    }
  }

    @Get
    @Path({"/cliente"/*, "/cliente/{acceptType}"*/}) // /{acao}
    public List<Cliente> pesquisar(Cliente cliente, String acceptType) {
        List<Cliente> clientes = clienteDomSvc.pesquisarPor(cliente); //
            String msgWarning = "Nenhum Cliente encontrado com o filtro informado!";
        if ("AJAX".equalsIgnoreCase(acceptType) ) {
            if (clientes == null || clientes.isEmpty() ) {
            //    msgWarning = "{mensagem: '" + msgWarning + "'}";
                result.use(Results.http() ).setStatusCode(400 /*, msgWarning*/);
                result.use(Results.representation() ).from(msgWarning).serialize();//
                result.use(Results.http() ).body(msgWarning).sendError(400 , msgWarning)/*404  */; // 
            } else {
                result.use(Results.representation() ).from(clientes).serialize();
            }
        } else {
            if (clientes == null || clientes.isEmpty() ) {
                result.include("msgErro", msgWarning);
            }
            result.redirectTo(ClienteController.class).manter(cliente);
        }
        
      return clientes;
    }
    @Post()
    @Path({"/cliente", "/cliente/{acceptType}"}) // /{acao}
    public void incluir(Cliente cliente, String acceptType) {
        cliente = clienteDomSvc.adiciona(cliente); //
        final String msgSucesso = "Cliente incluido com sucesso!";
        if ("AJAX".equalsIgnoreCase(acceptType) ) {
          result.use(Results.http() ).setStatusCode(201/*, msgSucesso*/);
          result.use(Results.representation() ).from(cliente).serialize();
        } else {
            result.include("msgSucesso", msgSucesso);
          result.redirectTo(ClienteController.class).manter(null);
      }
    }

  @Delete() @Consumes//(value = {"application/json;charset=UTF-8", "application/x-www-form-urlencoded; charset=UTF-8", "application/json;charset=ISO-8859-1"}), options=NoRootSerialization;charset=utf-8
  @Path({"/cliente", "/cliente/{acceptType}"})
  public void excluir(Cliente cliente, String acceptType) {
    try {
        clienteDomSvc.excluir(cliente);  //
        if ("AJAX".equalsIgnoreCase(acceptType) ) {
              result.use(Results.representation() ).from(cliente).serialize();
        }else {
          result.include("msgSucesso", "Cliente excluido com sucesso!");
      }
    } catch (Exception e) {
        final String msgWarning = "Cliente não existente com id informado!" ;
        if ("AJAX".equalsIgnoreCase(acceptType) ) {
                result.use(Results.http() ).setStatusCode(400 /*, msgWarning*/);
                result.use(Results.representation() ).from(msgWarning).serialize();//
//            result.use(Results.http() ).body(msgFalha).sendError(404, msgFalha); // return ;
        } else {
            result.include("msgErro", msgWarning );
        }
    }
    if (!"AJAX".equalsIgnoreCase(acceptType) ) {
        result.redirectTo(ClienteController.class).manter(null);
    }
  }

    @Put() @Consumes//(value={"application/json;charset=UTF-8", "application/x-www-form-urlencoded; charset=UTF-8", "application/json;charset=ISO-8859-1"})//, options=WithoutRoot.class;charset=utf-8
    @Path({"/cliente", "/cliente/{acceptType}"}) // /{acao}
    public void alterar(Cliente cliente, String acceptType) {
    try {
        clienteDomSvc.alterar(cliente);  //
        if ("AJAX".equalsIgnoreCase(acceptType)) {
            result.use(Results.representation()).from(cliente).serialize();
        } else {
            result.include("msgSucesso", "Cliente alterado com sucesso!");
      }
    } catch (Exception e) {
        final String msgWarning = "Cliente não existente com id informado!" ;
        if ("AJAX".equalsIgnoreCase(acceptType) ) {
                result.use(Results.http() ).setStatusCode(400 /*, msgWarning*/);
                result.use(Results.representation() ).from(msgWarning).serialize();//
//            result.use(Results.http() ).body(msgFalha).sendError(404, msgFalha); // return ;
        } else {
            result.include("msgErro", msgWarning );
        }
    }
    if (!"AJAX".equalsIgnoreCase(acceptType) ) {
        result.redirectTo(ClienteController.class).manter(null);
    }
  }

}
