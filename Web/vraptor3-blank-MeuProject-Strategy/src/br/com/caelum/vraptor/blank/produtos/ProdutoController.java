package br.com.caelum.vraptor.blank.produtos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import appServidora.domain.model.Produto;
import appServidora.domain.service.ProdutoSrvcFcd;
import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import static br.com.caelum.vraptor.view.Results.json;
import static br.com.caelum.vraptor.view.Results.xml;

@Resource
public class ProdutoController {

    @Autowired
    private  Result result; // final

    private ProdutoSrvcFcd produtoSrvcFcd;

    public ProdutoController(ProdutoSrvcFcd produtoSrvcFcd) { //, Result result, Validator validator,
        this.produtoSrvcFcd = produtoSrvcFcd;
    }

    public List<Produto> lista() {
        System.out.println("Retornando a List para a View!!");
        return produtoSrvcFcd.listaTodos(); //result.include("produtos", );
    }

    public void adiciona(Produto produto/*, String produtXYZ*/) {
    //      System.out.println("Param: " + produtXYZ);  
        produtoSrvcFcd.adiciona(produto);  //
        result.redirectTo(ProdutoController.class).lista();
    }

    @Post()// @Consumes(value={"application/json", "application/x-www-form-urlencoded"})//
    @Path({"/produto", "/produto/{acceptType}"}) // /{acao}
    public void incluir(Produto produto, String acceptType/**/) {
          System.out.println("Param acceptType: " + acceptType);  
        produtoSrvcFcd.adiciona(produto);  //
        final String msgSucesso = "Produto incluido com sucesso!";
        if (acceptType != null && /* "AJAX"*/ !"FULL".equalsIgnoreCase(acceptType) ) {
        //  result.on(Exception.class).use(Results.http() ).sendError(417, "Falha de validação!");
          result.use(Results.http() ).setStatusCode(201/*, msgSucesso*/);
          result.use(Results.representation()/*json() */).from(produto).serialize();
        }else {
            result.include("msgSucesso", msgSucesso);
            result.redirectTo(ProdutoController.class).manter();
        }
    }
  @Get
  @Path({"/produto/{produto.id}", "/produto/{produto.id}/{acao}"}) // /{acao}
  public/* Produto */void visualiza(Produto produto, String acao) {
    try {
      produto = produtoSrvcFcd.obterPeloId(produto/*.getId() */);
        if ("AJAX".equalsIgnoreCase(acao) ) {
            result.use(Results.representation()/*json() */).from(produto).serialize();
        }
        result.include("produto", produto); // 
    } catch (Exception e) {
        final String msgFalha = "Produto não encontrado com id informado!" ;
        if ("AJAX".equalsIgnoreCase(acao) ) {
            result.use(Results.http() ).body(msgFalha).sendError(404, msgFalha); // 
        } else {
            result.include("msgErro", msgFalha );
        }
    }
    if (!"AJAX".equalsIgnoreCase(acao) ) {
        result.redirectTo(ProdutoController.class).manter();
    }
  }

  @Delete() @Consumes//(value = {"application/json;charset=UTF-8", "application/x-www-form-urlencoded; charset=UTF-8", "application/json;charset=ISO-8859-1"}), options=NoRootSerialization;charset=utf-8
  @Path({"/produto", "/produto/{acceptType}"})
  public void excluir(Produto produto, String acceptType) {
        produtoSrvcFcd.excluir(produto);  //
        if (acceptType != null && /* "AJAX"*/ !"FULL".equalsIgnoreCase(acceptType) ) {
              result.use(json() ).from(produto).serialize();
        }else {
          result.redirectTo(ProdutoController.class).manter();
      }
  }

  @Put() @Consumes//(value={"application/json;charset=UTF-8", "application/x-www-form-urlencoded; charset=UTF-8", "application/json;charset=ISO-8859-1"})//, options=WithoutRoot.class;charset=utf-8
  @Path({"/produto", "/produto/{acceptType}"}) // 
  public void alterar(Produto produto, String acceptType) {
          produtoSrvcFcd.alterar(produto);  //
        if (acceptType != null && /* "AJAX"*/ !"FULL".equalsIgnoreCase(acceptType) ) {
              result.use(Results.representation() ).from(produto).serialize();
        }else {
          result.redirectTo(ProdutoController.class).manter();
      }
  }

    public void novo() {
        System.out.println("Entrou em 'novo()'!!");  
    }

    public List<Produto> manter() {
        System.out.println("Entrou em 'manter()'!!");
        return produtoSrvcFcd.listaTodos();
    }
}
