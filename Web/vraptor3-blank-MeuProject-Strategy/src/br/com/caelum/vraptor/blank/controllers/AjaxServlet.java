/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.vraptor.blank.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author derlonaliendres
 */
public class AjaxServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    //    try (
                PrintWriter out = response.getWriter();//) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AjaxServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AjaxServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
     //   }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            System.out.println("br.com.caelum.vraptor.blank.controllers.AjaxServlet.doPut()");
        final boolean ehAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With") );
        if (!ehAjax ) {
           request.setAttribute("COD_ACESSO_SERVICO", request.getParameter("cliente.nome") );
            request.setAttribute("snaFuncao", request.getParameter("snaFuncao") );
            request.setAttribute("pagFuncnalid", request.getParameter("pagFuncnalid") );

            request.getRequestDispatcher("/cliente").include(request, response); //
            request.getRequestDispatcher("/cliente/manter").forward(request, response); //
        } else {
            System.out.println("request.getContentType: " + request.getContentType());
            String body = inputStreamToString(request.getInputStream());
            System.out.println("body: " + body);
            request.getRequestDispatcher("/cliente").include(request, response); //
            doAjax(response);
        }
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("...controllers.AjaxServlet.doPost()");
        final boolean ehAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With") );
        if (!ehAjax ) {
        processRequest(request, response);

            request.getRequestDispatcher("/cliente").include(request, response); //
            request.getRequestDispatcher("/cliente/manter").forward(request, response); //
        } else {
            System.out.println("request.getContentType: " + request.getContentType());
            String body = inputStreamToString(request.getInputStream());
            System.out.println("body: " + body);
            request.getRequestDispatcher("/cliente").include(request, response); //
            doAjax(response);
        }
    }

        @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            System.out.println("br.com.caelum.vraptor.blank.controllers.AjaxServlet.doPut()");
        final boolean ehAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With") );
        if (!ehAjax ) {
           request.setAttribute("COD_ACESSO_SERVICO", request.getParameter("COD_ACESSO_SERVICO") );
            request.setAttribute("snaFuncao", request.getParameter("snaFuncao") );
            request.setAttribute("pagFuncnalid", request.getParameter("pagFuncnalid") );

            request.getRequestDispatcher("/cliente").include(request, response); //
            request.getRequestDispatcher("/cliente/manter").forward(request, response); //
        } else {
            System.out.println("request.getContentType: " + request.getContentType());
            String body = inputStreamToString(request.getInputStream());
            System.out.println("body: " + body);
            request.getRequestDispatcher("/cliente").include(request, response); //
            doAjax(response);
        }
    }

        @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("br.com.caelum.vraptor.blank.controllers.AjaxServlet.doDelete()");
        System.out.println("request.getQueryString: " + request.getQueryString());
        final boolean ehAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With") );
        final Map<String, String> parameterMap = request.getParameterMap();
        final Set entrySet = parameterMap.entrySet();
        for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        if (!ehAjax ) {
           request.setAttribute("COD_ACESSO_SERVICO", request.getParameter("COD_ACESSO_SERVICO") );
            request.setAttribute("snaFuncao", request.getParameter("snaFuncao") );
            request.setAttribute("pagFuncnalid", request.getParameter("pagFuncnalid") );

            request.getRequestDispatcher("/cliente").include(request, response); //
            request.getRequestDispatcher("/cliente/manter").forward(request, response); //
        } else {
            System.out.println("request.getContentType: " + request.getContentType());
            String body = inputStreamToString(request.getInputStream());
            System.out.println("body: " + body);
            request.getRequestDispatcher("/produto").include(request, response); //
            doAjax(response);
        }
    }

    private void doAjax(HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            PrintWriter writer = response.getWriter();
        //    final ObjectMapper jsonMapper = new ObjectMapper();

        //    writer.write(jsonMapper.writeValueAsString(novaNotaFiscal) );
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName() ).log(Level.SEVERE, null, ex);
        }
    }

  private static String inputStreamToString(InputStream inputStream) {
      Scanner scanner = new Scanner(inputStream, "UTF-8");
      return scanner.hasNext() ? scanner.useDelimiter("\\A").next() : "";
  }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
