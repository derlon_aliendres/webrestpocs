package br.com.caelum.vraptor.blank.direitos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;
import appServidora.domain.model.specifications.DireitoSpecification;
import appServidora.domain.model.specifications.EnumDireitoSpec;
import appServidora.domain.service.CidadaoSrvcFcd;
import appServidora.domain.service.DireitoDomSrvc;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class DireitoController {

    @Autowired
    private  Result result; // final

    private DireitoDomSrvc direitoDomSrvc;
    private CidadaoSrvcFcd cidadaoSrvcFcd;

	private List<String> lstParticipsDoCid = new ArrayList<String>();

    public DireitoController(DireitoDomSrvc direitoDomSrvc, CidadaoSrvcFcd cidadaoSrvcFcd) { //, Result result, Validator validator,
          this.direitoDomSrvc = direitoDomSrvc;
          this.cidadaoSrvcFcd = cidadaoSrvcFcd;
    }

    public List<Cidadao> lista() {
          System.out.println("Retornando a List<Cidadao> para a View .listaTodos()!!");
          return direitoDomSrvc.listaTodos(); //result.include("produtos", );
    }

    public void adiciona(Cidadao cidadao) {
//          direitoDomSrvc.exercerdo(cidadao, oVotoDireitoSpec)(produto);  //adiciona
          direitoDomSrvc.adiciona(cidadao);  //(cidadao, oVotoDireitoSpec)
          result.redirectTo(DireitoController.class).lista();
    }

    public void manter() {
          System.out.println("Entrou em '[Direito].manter()'!!");  
    }

    public /*String*/void exercer(Cidadao cidadao, int tipoDireito) {
    	DireitoSpecification oVotoDireitoSpec = direitoDomSrvc.obterDireitoSpecDo(tipoDireito);
    	String voto = null;
		try {
			voto = direitoDomSrvc.exercerDo(cidadao, oVotoDireitoSpec);
		} catch (Exception e) {
			// e.printStackTrace();
			result.include("msgErro", e.getMessage() );			
		}
    	
    	List<String> votosList = new ArrayList<String>();
    	votosList.add(voto);
    	result.include("msgSucesso", voto);
    	result.include("votosList", votosList);
//         return ; //adiciona
    }

    public /*String*/void obter(Cidadao cidadao, int tipoDireito) {
    /*	DireitoSpecification oVotoDireitoSpec = direitoDomSrvc.obterDireitoSpecDo(tipoDireito);
    	String voto = null;
		try {
			voto = direitoDomSrvc.exercerDo(cidadao, oVotoDireitoSpec);
		} catch (Exception e) {
			// e.printStackTrace();
			result.include("msgErro", e.getMessage() );			
		}*/
    	 //  new ArrayList<String>()
    	List<AbstractDireito> votosList = null;
		try {
			votosList = direitoDomSrvc.obterDo(cidadao, tipoDireito);
		} catch (Exception e) {
			// e.printStackTrace();
			result.include("msgErro", e.getMessage() );			
		}
    //	votosList.add(voto);
    //	result.include("msgSucesso", voto);
    	result.include("direitosList", votosList);
    //     return ; //adiciona
    }


    public List<Cidadao> listar() {
          System.out.println("Retornando a List<Cidadao> para a View .listarTodos()!!");
          return cidadaoSrvcFcd.listaTodos(); //result.include("produtos", );
    }

    public void manterPes() {
          System.out.println("Entrou em '[Direito].manterPes()'!!");  
    }

   @Post
   public void adicionarPes(Cidadao cidadao) {
//          direitoDomSrvc.exercerdo(cidadao, oVotoDireitoSpec)(produto);  //adiciona
    	cidadaoSrvcFcd.adiciona(cidadao);  //(cidadao, oVotoDireitoSpec)
    	result.redirectTo(DireitoController.class).listar();
    }

	@Get
    public Cidadao criarLicenca(Integer id) {
    	System.out.println(id);
    	result.include("EnumDireitoSpec", EnumDireitoSpec.values() ); // BASICA
    	return cidadaoSrvcFcd.obterPelo(id);
	}

	@Post
    public Cidadao/*void*/ criarLicenca(Cidadao cidadao, EnumDireitoSpec enumDireitoSpec) {
    	try {
			AbstractDireito oVotoDireitoSpec = direitoDomSrvc.obterDireitoSpecDo(enumDireitoSpec, cidadao);
		} catch (Exception e) {
			// e.printStackTrace();
			result.include("msgErro", e.getMessage() );			
		}
    	final Cidadao cidadao2 = criarLicenca(cidadao.getId() );/*cidadaoSrvcFcd.obterPelo( )*/
		final List<AbstractDireito> lstDireitosDoCid = direitoDomSrvc.obterTodosDo(cidadao2);
		result.include("direitosDoCidList", lstDireitosDoCid );
    	return cidadao2;
	}

	@Get
    public AbstractDireito participarEvento(Integer id) {
    	AbstractDireito carteirHab = direitoDomSrvc.obterPelo(id);
		final List<String> lstDireitosDoCid = direitoDomSrvc.obterEventosDa(carteirHab);
		result.include("Eventos", lstDireitosDoCid );
    	
    	return carteirHab;
	}

	@Post
    public AbstractDireito participarEvento(Integer idDireito, String eventos) {// AbstractDireito abstractDireito
    	AbstractDireito carteirHab = participarEvento(idDireito);
		lstParticipsDoCid.add("Parabens, vc esta guiando a sua " + eventos  + "!!!");
		result.include("Participacoes", lstParticipsDoCid );
    	
    	return carteirHab;
	}

    public /*String*/void obterLicenca(Cidadao cidadao, EnumDireitoSpec enumDireitoSpec) {
    	String voto = null;
		try {
			voto =direitoDomSrvc.exercerDo(cidadao, null) ; // oVotoDireitoSpec
		} catch (Exception e) {
			// e.printStackTrace();
			result.include("msgErro", e.getMessage() );			
		}
    	
    	List<String> votosList = new ArrayList<String>();
    	votosList.add(voto);
    	result.include("msgSucesso", voto);
    	result.include("votosList", votosList);
//         return ; //adiciona
    }
}
