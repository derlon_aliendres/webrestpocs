package appServidora.domain.repository;

import appServidora.domain.model.Cliente;
import java.util.ArrayList;
import java.util.List;

import appServidora.domain.model.Entidade;

public class ClienteRepo<T extends Cliente> extends GenericRepo<Cliente> {

//	private ArrayList<T> lstEntidades/* = new ArrayList<Produto>()*/;
//	private Class classe;

	public ClienteRepo(ArrayList<Cliente> lstEntidades) {
		super(lstEntidades);
//		classe = T;
	}

    @Override
    public List<Cliente> pesquisarPor(Cliente filtroCliente) {
        List<Cliente> lstClientes = new ArrayList<Cliente>(1);
        if (lstEntidades != null ) {
            for (Cliente eachCliente: getLstEntidades() ) {
                final String nome = (filtroCliente != null? filtroCliente.getNome(): null);
                if (nome == null || "".equals(nome) || eachCliente.getNome().toLowerCase().contains(nome.toLowerCase() )) {
                    lstClientes.add(eachCliente);
                }
            }
        }

        return lstClientes;
    }
}
