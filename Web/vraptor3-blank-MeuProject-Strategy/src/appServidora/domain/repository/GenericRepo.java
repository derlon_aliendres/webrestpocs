package appServidora.domain.repository;

import java.util.ArrayList;
import java.util.List;

import appServidora.domain.model.Entidade;

public abstract class GenericRepo<T extends Entidade> {

	protected ArrayList<T> lstEntidades/* = new ArrayList<Produto>()*/;
//	private Class classe;

	public GenericRepo(ArrayList<T> lstEntidades) {
		super();
		this.setLstEntidades(lstEntidades);
//		classe = T;
	}
   
    public T adiciona(T entidade) {
        final Integer id = lstEntidades.isEmpty()? 0 : (lstEntidades.get(lstEntidades.size() - 1).getId() );
    	entidade.setId(id + 1);
    	getLstEntidades().add( (T)entidade);
            return entidade;
    }

    public List<T> obterTodos() {
        return getLstEntidades();
    }

    public/* Entidade */T obterPelo(Integer id) {
        for (T entidade : lstEntidades) {
            if (entidade.getId().equals(id)) {

                return entidade;
            }
        } 
            return null;
    }

    public T obterPeloId(T clienteComId) {
        return lstEntidades.get(lstEntidades.indexOf(clienteComId) );
    }

    public abstract List<T> pesquisarPor(T filtroEntidade);

	public/* Entidade */T alterar(T entidade) {
		return getLstEntidades().set(getLstEntidades().indexOf(entidade), entidade);
	}

	public/* Entidade */T excluir(T entidade) {
		return getLstEntidades().remove(getLstEntidades().indexOf(entidade) );
	}

	// daki em diante: espec�ficos da Linguagem Prog.
	protected ArrayList<T> getLstEntidades() {
		return lstEntidades;
	}

	protected void setLstEntidades(ArrayList<T> lstEntidades) {
		this.lstEntidades = lstEntidades;
	}
}
