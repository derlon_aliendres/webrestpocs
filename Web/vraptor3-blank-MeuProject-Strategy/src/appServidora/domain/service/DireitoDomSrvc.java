package appServidora.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;

import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;
import appServidora.domain.model.DirigirDireito;
import appServidora.domain.model.Entidade;
import appServidora.domain.model.Produto;
import appServidora.domain.model.VotoDireito;
import appServidora.domain.model.specifications.DireitoSpecification;
import appServidora.domain.model.specifications.DirigirDireitoSpec;
import appServidora.domain.model.specifications.EnumDireitoSpec;
import appServidora.domain.model.specifications.VotoDireitoSpec;
import appServidora.domain.repository.GenericRepo;
import appServidora.domain.repository.GenericRepoImpl;

//@Service
@Component
public class DireitoDomSrvc {

//	@
	private ProdutoSrvcFcd produtoSrvcFcd;
   private static final ArrayList<Cidadao> lstCidadaos = new ArrayList<Cidadao>(); // VotoDireito
   private static final ArrayList<AbstractDireito> lstDireitos = new ArrayList<AbstractDireito>(); // 
    public GenericRepo<Cidadao> cidadaoRepo; // VotoDireito
    public GenericRepo<AbstractDireito> direitoRepo; // VotoDireito

    public DireitoDomSrvc(ProdutoSrvcFcd produtoSrvcFcd) {
		cidadaoRepo = new GenericRepoImpl<Cidadao>(lstCidadaos); // VotoDireito
		direitoRepo = new GenericRepoImpl<AbstractDireito>(lstDireitos); // 
		this.produtoSrvcFcd = produtoSrvcFcd;
	}
		
	public void adiciona(Cidadao cidadao) {
		cidadaoRepo.adiciona( cidadao);
    }
	public List<Cidadao> listaTodos() { // VotoDireito
        return cidadaoRepo.obterTodos();
    }

		
	public void adiciona(AbstractDireito direito) throws Exception {
		for (AbstractDireito umDireito : lstDireitos) {
			if (direito.getCidadao().equals(umDireito.getCidadao() )
					&& direito.getDescricao().equalsIgnoreCase(umDireito.getDescricao() ) ) {
				throw new Exception("Cidadao ja tem essa Licença");
			}
		}
		direitoRepo.adiciona( direito);
    }
	public String exercerDo(Cidadao cidadao, DireitoSpecification oVotoDireitoSpec) throws Exception {
		AbstractDireito voto = (AbstractDireito) oVotoDireitoSpec.exercer(cidadao);
		adiciona( voto); // voto
		return voto.exercutarLegado();
	}

	public ArrayList<AbstractDireito> obterDo(Cidadao cidadao, int codTipoDireito) throws Exception {
		AbstractDireito voto = (AbstractDireito) obterDireitoDo(codTipoDireito, cidadao);
		adiciona(voto.exercer() ); // voto
		return lstDireitos;
	}

	public List<Cidadao> obterTodosCidadaos() {
		
		return cidadaoRepo.obterTodos();
	}

	public DireitoSpecification obterDireitoSpecDo(int tipo) {
		switch (tipo) {
			case 0: 
				return new VotoDireitoSpec();
			//	break;
	
			default:
				return new DirigirDireitoSpec();
			//	break;
		}
	}

	public AbstractDireito obterDireitoDo(int tipo, Cidadao paraCidadao) {
		switch (tipo) {
			case 0: 
				return new VotoDireitoSpec(paraCidadao);
			//	break;
	
			default:
				DirigirDireitoSpec dirigirDireitoSpec = null;
				try {
					dirigirDireitoSpec = new DirigirDireitoSpec(paraCidadao);
				} catch (Exception e) {
					System.out.println("[INFO] Excessao necessario p/a nova implementação!"); // TODO 
					// e.printStackTrace();
				}
				return dirigirDireitoSpec;
			//	break;
		}
	}

	public AbstractDireito obterDireitoSpecDo(EnumDireitoSpec tipoDireito, Cidadao paraCidadao) throws Exception {
		final AbstractDireito direito = tipoDireito.obterDireitoDo(paraCidadao);
		adiciona(direito);
		
		return direito;
	}

	public List<AbstractDireito> obterTodosDo(Cidadao paraCidadao) {
		List<AbstractDireito> lstDireitosDoCid = new ArrayList<AbstractDireito>(4);
		for (AbstractDireito direito : lstDireitos) {
			if (direito.getCidadao().equals(paraCidadao) ) {
				lstDireitosDoCid.add(direito);
			}
		}
		return lstDireitosDoCid;
	}

	public AbstractDireito obterPelo(Integer id) {
		return (AbstractDireito) direitoRepo.obterPelo(id);
	}

	private AbstractDireito obter(AbstractDireito carteirHab) {
		return obterPelo(carteirHab.getId());
	}

	public List<String> obterEventosDa(AbstractDireito carteirHab) {
		List<?> ents = new ArrayList(4);
		/*final AbstractDireito */carteirHab = obter(carteirHab);
		if (carteirHab instanceof AbstractDireito) {
			ents = (List<?>)produtoSrvcFcd.listaTodos();
		}
		List<String> ret = obterDescrsDas(ents);
		return ret;
	}

	private List<String> obterDescrsDas(List<?> ents) {
		List<String> ret = new ArrayList<String>(4);
		for (Object ent : ents) {
			if (ent instanceof Produto)
				ret.add( ( ( (Produto)ent).getDescricao() ) );
		}
		return ret;
	}
}
