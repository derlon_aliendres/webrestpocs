package appServidora.domain.service;

import appServidora.domain.model.Cliente;
import appServidora.domain.repository.ClienteRepo;
import java.util.ArrayList;
import java.util.List;


import br.com.caelum.vraptor.ioc.Component;

import appServidora.domain.repository.GenericRepo;

//@Service
@Component
public class ClienteDomSvc {

    private static final ArrayList<Cliente> lstEntidades = new ArrayList<Cliente>();
    private static List<Cliente> lstEntsFiltrada = new ArrayList<Cliente>();
    public ClienteRepo<Cliente> genericRepo;

    public ClienteDomSvc() {
		genericRepo = new ClienteRepo/*<Cliente>*/(lstEntidades);
	}
		
	public Cliente adiciona(Cliente cliente) {
        return genericRepo.adiciona(cliente);
    }

    public Cliente obterPelo(Integer id) {
            return genericRepo.obterPelo(id);
    }

    public Cliente obterPeloId(Cliente clienteComId) {
            return genericRepo.obterPeloId(clienteComId);
    }

    public List<Cliente> pesquisarPor(Cliente filtroCliente) {
        lstEntsFiltrada = genericRepo.pesquisarPor(filtroCliente);
        return lstEntsFiltrada;
    }
		
    public void alterar(Cliente cliente) {
        genericRepo.alterar(cliente);
    }

		
    public void excluir(Cliente cliente) {
        genericRepo.excluir(cliente);
    }

    public List<Cliente> listaTodos() {
        lstEntsFiltrada = genericRepo.obterTodos();
        return lstEntsFiltrada;
    }

    /**
     * @return the lstEntsFiltrada
     */
    public static List<Cliente> getLstEntsFiltrada() {
        return lstEntsFiltrada;
    }
   
}
