package appServidora.domain.service;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;

import appServidora.domain.model.Cidadao;
import appServidora.domain.repository.GenericRepo;
import appServidora.domain.repository.GenericRepoImpl;

//@Service
@Component
public class CidadaoSrvcFcd {

    private static final ArrayList<Cidadao> lstEntidades = new ArrayList<Cidadao>();
    public GenericRepo<Cidadao> genericRepo;

    public CidadaoSrvcFcd() {
		genericRepo = new GenericRepoImpl<Cidadao>(lstEntidades);
	}
		
	public void adiciona(Cidadao produto) {
		genericRepo.adiciona(produto);
    }
	public List<Cidadao> listaTodos() {
        return genericRepo.obterTodos();
    }

	public Cidadao obterPelo(Integer id) {
		return (Cidadao) genericRepo.obterPelo(id);
	}
   

}
