package appServidora.domain.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.caelum.vraptor.ioc.Component;

import appServidora.domain.model.Produto;
import appServidora.domain.repository.GenericRepo;
import appServidora.domain.repository.GenericRepoImpl;

//@Service
@Component
public class ProdutoSrvcFcd {

    private static final ArrayList<Produto> lstEntidades = new ArrayList<Produto>();
    public GenericRepo<Produto> genericRepo;

    public ProdutoSrvcFcd() {
		genericRepo = new GenericRepoImpl<Produto>(lstEntidades);
	}
		
	public void adiciona(Produto produto) {
		genericRepo.adiciona(produto);
    }

    public Produto obterPeloId(Produto produto) {
         return genericRepo.obterPeloId(produto);
    }

    public List<Produto> listaTodos() {
        return genericRepo.obterTodos();
    }

    public Produto excluir(Produto produto) {
        return genericRepo.excluir(produto);
    }

    public Produto alterar(Produto produto) {
        return genericRepo.alterar(produto);
    }
   
}
