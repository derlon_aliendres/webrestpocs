package appServidora.domain.model;

import java.util.Date;


public abstract class AbstractDireito extends Entidade {
	
	private Date dtEmissao;
	private Cidadao cidadao;
	private String descricao;
	public /*final*/ Integer IDADE_DE_MAIOR;

	public AbstractDireito(Date dtEmissao) {
		this.setDtEmissao(dtEmissao);
		setDescricao(this.getClass().getSimpleName().replace("DireitoSpec", ""));
	}

	public String getDescricao() {
		return descricao;
	}

	public AbstractDireito(Date dtEmissao, Cidadao cidadao) {
		this(dtEmissao);
		this.setCidadao(cidadao);
	}

	protected void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public AbstractDireito exercer() throws Exception {
		if (getCidadao().getIdade() < IDADE_DE_MAIOR)
			throw new Exception("Cidadao n�o tem idade suficiente p/execer esse Direito!");
		
		return this;
	}


	public abstract AbstractDireito exercer(Cidadao doCidadao) throws Exception;

	Date getDtEmissao() {
		return dtEmissao;
	}

	void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public Cidadao getCidadao() {
		return cidadao;
	}

	protected void setCidadao(Cidadao cidadao) {
		this.cidadao = cidadao;
	}

	public String exercutarLegado() {
		
		return getDescricao() + " efetuado com Sucesso!"; // Voto
	}

	
}
