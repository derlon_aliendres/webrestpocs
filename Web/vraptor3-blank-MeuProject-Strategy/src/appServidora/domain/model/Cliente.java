/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appServidora.domain.model;

/**
 *
 * @author derlonaliendres
 */
public class Cliente extends Entidade{

    	private String nome;
	
	private String login;

    public Cliente() {
    }

    public Cliente(String nome, String login) {
        this.nome = nome;
        this.login = login;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }
}
