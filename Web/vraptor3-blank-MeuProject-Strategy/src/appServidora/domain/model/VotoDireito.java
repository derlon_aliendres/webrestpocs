package appServidora.domain.model;

import java.util.Date;

public class VotoDireito extends AbstractDireito {

	public VotoDireito(Date dtEmissao) {
		super(dtEmissao);
	}

	@Override
	public AbstractDireito exercer() {
		return this;
	}

	public VotoDireito(Date dtEmissao, Cidadao cidadao) {
		super(dtEmissao, cidadao);
	}
	
	public AbstractDireito exercer(Cidadao doCidadao) throws Exception {
		if (doCidadao.getIdade() < IDADE_DE_MAIOR)
			throw new Exception("Cidadao n�o tem idade suficiente p/execer esse Direito!");
		AbstractDireito votoDireito = new VotoDireito(new Date() ); // new VotoDireito()
		
		return votoDireito;
	}

}
