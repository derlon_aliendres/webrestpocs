package appServidora.domain.model.specifications;

import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;

public interface DireitoSpecification {

	public AbstractDireito exercer(Cidadao cidadao) throws Exception;

}
