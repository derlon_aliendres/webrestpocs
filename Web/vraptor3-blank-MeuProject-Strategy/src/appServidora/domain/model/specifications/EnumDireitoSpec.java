package appServidora.domain.model.specifications;

import java.util.Date;

import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;
import appServidora.domain.model.VotoDireito;

public enum EnumDireitoSpec {
	VOTAR("V", "Votar") {    
		public AbstractDireito obterDireitoDo(Cidadao cidadao) {
			return new VotoDireito(new Date(), cidadao);
		}    
     //   public String fazNada(){return "m�todo da constante";}    
    },  
	/**
	 */
	DIRIGIR("D", "Dirigir") {
		@Override
		public AbstractDireito obterDireitoDo(Cidadao cidadao) throws Exception {
			
			return new DirigirDireitoSpec(cidadao);
		}
	},
	/**
	 
	BASICA("B", "Basica")*/;

	// Definicao de constante
	private final String codDireito;

	private final String nomeDireito;

	EnumDireitoSpec(String codDireito, String nomeDireito) {
		this.codDireito = codDireito;
		this.nomeDireito = nomeDireito;
	}

	public abstract AbstractDireito obterDireitoDo(Cidadao cidadao) throws Exception;
		
	
	public String getCodDireito() {
		return this.codDireito;
	}

	public String getNomeDireito() {
		return this.nomeDireito;
	}
}
