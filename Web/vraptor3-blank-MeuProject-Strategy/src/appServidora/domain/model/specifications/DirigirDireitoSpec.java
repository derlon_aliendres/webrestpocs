package appServidora.domain.model.specifications;

import java.util.Date;

import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;
import appServidora.domain.model.DirigirDireito;
import appServidora.domain.model.VotoDireito;

public class DirigirDireitoSpec extends AbstractDireito implements DireitoSpecification {

	private void init() {
		IDADE_DE_MAIOR = 18;
	}

	public DirigirDireitoSpec() {
		super(new Date() ); // ->>Overload de Conveni�ncia p/ Legado.
		init();
	}

	public DirigirDireitoSpec(Cidadao cidadao) throws Exception {
		super(new Date(), cidadao);
		init();
		exercer();
	}
	
/*	@Override
	public AbstractDireito exercer() throws Exception {
		if (getCidadao().getIdade() < IDADE_DE_MAIOR)
			throw new Exception("Cidadao não tem idade suficiente p/execer esse Direito!");
		
		return this;
	}*/
	public AbstractDireito exercer(Cidadao doCidadao) throws Exception {
		setCidadao(doCidadao);
		
		return exercer();
	}

}
