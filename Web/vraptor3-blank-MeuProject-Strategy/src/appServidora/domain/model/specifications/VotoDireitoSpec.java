package appServidora.domain.model.specifications;

import java.util.Date;

import appServidora.domain.model.AbstractDireito;
import appServidora.domain.model.Cidadao;
import appServidora.domain.model.Entidade;
import appServidora.domain.model.VotoDireito;

public class VotoDireitoSpec extends AbstractDireito implements DireitoSpecification {
	private void init() {
		IDADE_DE_MAIOR = 16;
	}
	
	public VotoDireitoSpec() {
		super(new Date() ); // ->>Overload de Conveniencia p/ Legado.
		init();
	}
	public VotoDireitoSpec(Cidadao cidadao) {
		super(new Date(), cidadao);
		init();
	}

/*	@Override
	public AbstractDireito exercer() throws Exception {
		if (getCidadao().getIdade() < IDADE_DE_MAIOR)
			throw new Exception("Cidadao nao tem idade suficiente p/execer esse Direito!");
		VotoDireitoSpec votoDireito = this; // new VotoDireito(new Date() )
		
		return votoDireito;
	}*/

	public AbstractDireito exercer(Cidadao doCidadao) throws Exception {
		setCidadao(doCidadao);
		
		return exercer();
	}

}
