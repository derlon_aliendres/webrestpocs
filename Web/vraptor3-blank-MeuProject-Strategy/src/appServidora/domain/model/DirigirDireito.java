package appServidora.domain.model;

import java.util.Date;

public class DirigirDireito extends AbstractDireito {

	private Date dtValidade;

	public DirigirDireito(Date dtEmissao) {
		super(dtEmissao);
	}

	public DirigirDireito(Date dtEmissao, Date dtValidade) {
		super(dtEmissao);
		this.setDtValidade(dtValidade);
	}

	public Date getDtValidade() {
		return dtValidade;
	}

	public void setDtValidade(Date dtValidade) {
		this.dtValidade = dtValidade;
	}

	public AbstractDireito exercer(Cidadao doCidadao) throws Exception {
		if (doCidadao.getIdade() < IDADE_DE_MAIOR)
			throw new Exception("Cidadao n�o tem idade suficiente p/execer esse Direito!");
		AbstractDireito votoDireito = new VotoDireito(new Date() ); // new VotoDireito()
		
		return votoDireito;
	}

}
