package any;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;

import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import testUtil.WeldJUnit4Runner;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(WeldJUnit4Runner.class)
public class EMConsumerTest {
	
	
    @Inject //@Resource	
    /*EMConsumer emConsumer;*/
    //@Resource	@Inject 
  /*  static */EMConsumer cRepo;

//    @PostConstruct@Inject //@Resource	
    public void init(EMConsumer emConsumer) {
        Logger.getLogger(EMConsumerTest.class.getName()).log(Level.INFO, "[init]");
        if (cRepo == null) {
            cRepo = emConsumer;
        }
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
                Logger.getLogger(EMConsumerTest.class.getName()).log(Level.INFO, "[setUpClass]");
//        if (cRepo == null) {
//            cRepo = emConsumer;
//        }
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
	
	@Test
	public void doWithPersistenceContextInjetado() {
            try {
                        Assert.assertNotNull("Falhou a injection do cRepo!", cRepo);
                        Assert.assertNotNull("Falhou a injection do EntityManager!", cRepo.em);
                        Assert.assertTrue(cRepo.doSomething());
            } catch (Exception ex) {
                Logger.getLogger(EMConsumerTest.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

    /**
     * Test of incluir method, of class EMConsumer.
     */
    @Test
    public void testIncluir() {
        System.out.println("incluir");
        id = 1l;
        Department department = new Department(/*id, */"Logistica");
        Department result = null;
        try {
            result = cRepo.incluir(department);
        } catch (Exception ex) {
            Logger.getLogger(EMConsumerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
//        id = result.getId();
        Logger.getLogger(EMConsumerTest.class.getName()).log(Level.INFO, "Id: "+ id);
        Department expResult = new Department(id);
        assertEquals("Falha ao tentar incluir entity!", expResult, result);
    }
    private static Long id;

    /**
     * Test of obterPelo method, of class EMConsumer.
     */
    @Test
    public void testObterPelo() {
        System.out.println("obterPelo");
        Department expResult = new Department(/*id, */"Logistica"); // //new Department(id); 
        Department result = null;
        try {
        /*    result = */cRepo.incluir(expResult);
            result = cRepo.obterPelo(expResult.getId() );
        } catch (Exception ex) {
            Logger.getLogger(EMConsumerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
    }
    
}
