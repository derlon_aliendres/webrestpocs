package any;

import java.io.Serializable;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import org.jboss.solder.core.ExtensionManaged;

//@Dependent//@ConversationScoped // @Default
//@Named
public class EMConsumer implements Serializable {

    @Inject
    Conversation conversation;
	private static final long serialVersionUID = 1L;

	private static final String cdiJpaEmJavaSE= "cdiJpaEmJavaSE";
//    @ExtensionManaged
//    @Produces
//        @PersistenceUnit(unitName=cdiJpaEmJavaSE) 
//    EntityManagerFactory emf;; //EntityManager em 
    @Inject
	@PersistenceContext() // unitName=cdiJpaEmJavaSE//  @ExtensionManaged
	EntityManager em;
	
	public EMConsumer() {
//            conversation.begin();
		System.out.println("*** EMConsumer Bean, default contructor, em injected=" + em);
	}
	
	public Boolean doSomething() {
		System.out.println("*** EMConsumer.doSomething, em injected =" + em);
            final boolean name = em==null?false:true;
		
		return name;
	}

    public Department incluir(Department department) {
        try {
            em.getTransaction().begin();
            em.persist(department);
            em.flush();
            System.out.println("[EMConsumer]incluido Department com Id: " + department.getId());
            return department;
        } finally {
        //    em.close();
        }
    }

    public Department obterPelo(Long id) {
        try {
            return em.find(Department.class, id);
        } finally {
        //    em.close();
        }
    }
}
