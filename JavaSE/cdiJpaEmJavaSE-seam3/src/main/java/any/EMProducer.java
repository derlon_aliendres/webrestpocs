package any;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.jboss.solder.core.ExtensionManaged;

    @Default
public class EMProducer {

    @Dependent
    @ExtensionManaged
    @Produces
    @PersistenceUnit(unitName="cdiJpaEmJavaSE") // 
    EntityManagerFactory emf;; //EntityManager em 

//    @Dependent
//    @ExtensionManaged
//    @Produces
//    @PersistenceContext() // unitName="cdiJpaEmJavaSE"
//    EntityManager em; // EntityManagerFactory emf;
 	   
//    @ConversationScoped
//    @Produces
//    public EntityManager gerar() {
//        
//        return (em!=null? em: emf.createEntityManager() );//emf.createEntityManager();
//    }
}
